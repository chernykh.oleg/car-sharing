name := "car-sharing"
version := "0.1"
scalaVersion := "2.13.5"

resolvers += "confluent-io" at "https://packages.confluent.io/maven/"

val AkkaVersion = "2.6.14"
val AkkaHttpVersion = "10.2.4"
libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-actor-typed" % AkkaVersion,
  "com.typesafe.akka" %% "akka-stream" % AkkaVersion,
  "com.typesafe.akka" %% "akka-http" % AkkaHttpVersion,
  "com.typesafe.akka" %% "akka-stream-testkit" % "2.6.14" % Test,
  "com.typesafe.akka" %% "akka-http-testkit" % "10.2.4" % Test,
)

libraryDependencies ++= Seq(
  "com.softwaremill.sttp.tapir" %% "tapir-core" % "0.18.0-M7",
  "com.softwaremill.sttp.tapir" %% "tapir-akka-http-server" % "0.18.0-M7",
  "com.softwaremill.sttp.tapir" %% "tapir-json-circe" % "0.18.0-M7",
  "com.softwaremill.common" %% "tagging" % "2.2.1",
  "com.softwaremill.sttp.tapir" %% "tapir-openapi-docs" % "0.18.0-M7",
  "com.softwaremill.sttp.tapir" %% "tapir-openapi-circe-yaml" % "0.18.0-M7",
  "com.softwaremill.sttp.tapir" %% "tapir-swagger-ui-akka-http" % "0.18.0-M7",
  "com.softwaremill.sttp.tapir" %% "tapir-redoc-akka-http" % "0.18.0-M7"
)

val circeVersion = "0.12.2"

libraryDependencies ++= Seq(
  "io.circe" %% "circe-core",
  "io.circe" %% "circe-generic",
  "io.circe" %% "circe-parser",
  "io.circe" %% "circe-generic-extras"
).map(_ % circeVersion)

libraryDependencies ++= Seq(
  "com.typesafe.slick" %% "slick" % "3.3.3",
  "org.slf4j" % "slf4j-nop" % "1.6.4",
  "com.typesafe.slick" %% "slick-hikaricp" % "3.3.3",
//  "org.postgresql" % "postgresql" % "9.3-1100-jdbc41"
  "org.postgresql" % "postgresql" % "42.1.1"
)

enablePlugins(ProtobufPlugin)

libraryDependencies ++= Seq(
  "com.github.t3hnar" %% "scala-bcrypt" % "4.3.0",
  "org.typelevel" %% "cats-core" % "2.3.0",
  "io.monix" %% "monix" % "3.3.0",
  "com.github.cb372" %% "scalacache-redis" % "0.28.0",
  "com.github.cb372" %% "scalacache-cats-effect" % "0.28.0",
  "com.github.cb372" %% "scalacache-circe" % "0.28.0",
  "io.scalaland" %% "chimney" % "0.6.1",
  "com.github.pureconfig" %% "pureconfig" % "0.15.0",
  "com.typesafe.scala-logging" %% "scala-logging" % "3.9.3",
  "ch.qos.logback" % "logback-classic" % "1.1.2",
  "org.apache.kafka" %% "kafka" % "2.8.0",
  "org.apache.kafka" % "kafka-clients" % "2.8.0",
  "io.confluent" % "kafka-protobuf-serializer" % "5.5.0",
  "com.google.protobuf" % "protobuf-java" % "3.15.8",
  "org.scalamock" %% "scalamock" % "4.4.0" % Test,
  "org.scalatest" %% "scalatest" % "3.2.2" % Test,
  "de.heikoseeberger" %% "akka-http-circe" % "1.36.0"
)

libraryDependencies ++= Seq(
  "com.dimafeng" %% "testcontainers-scala-scalatest" % "0.39.3" % "test",
  "com.dimafeng" %% "testcontainers-scala-postgresql" % "0.39.3" % "test",
)

fork in Test := true

import com.permutive.sbtliquibase.SbtLiquibase

enablePlugins(SbtLiquibase)
liquibaseUsername := "root"
liquibasePassword := "root"
liquibaseDriver := "org.postgresql.Driver"
liquibaseUrl := "jdbc:postgresql://localhost:5432/postgres"

scalacOptions ++= Seq(
  "-unchecked",
  "-feature",
  "-deprecation",
  "-explaintypes",
  "-language:postfixOps",
  "-language:higherKinds",
  "-language:implicitConversions",
  "-language:reflectiveCalls",
  "-Ywarn-extra-implicit",
  "-Ywarn-unused:params",
  "-Ymacro-annotations",
  "-Ybackend-parallelism",
  "12",
  "-Xmacro-settings:materialize-derivations"
)
