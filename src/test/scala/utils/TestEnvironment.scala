package utils

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.{HttpRequest, HttpResponse}
import akka.http.scaladsl.testkit.RouteTestTimeout
import akka.http.scaladsl.unmarshalling.{FromResponseUnmarshaller, Unmarshal}
import api.UsersApi.GetCarsToBookEndpoint.{GeoPoint, SearchArea}
import api.UsersApi.SignInEndpoint.UserRegistration
import monix.eval.Task
import monix.execution.Scheduler
import org.scalatest.{Assertion, Suite}

import scala.concurrent.duration.DurationInt

trait TestEnvironment { this: Suite =>

  implicit val actorSystem = ActorSystem()
  implicit val scheduler = Scheduler(actorSystem.dispatcher)

  implicit val defaultTimeout: RouteTestTimeout = RouteTestTimeout(5 seconds)

  val validRegistrationData: UserRegistration =
    UserRegistration("root42", "root42", "email@test.test", "1234567890")

  val validRegistrationData2: UserRegistration =
    UserRegistration("root4242", "root4242", "email42@test.test", "1234567842")

  val invalidRegistrationData: UserRegistration =
    UserRegistration("root", "root42", "email@test.test", "1234567890")

  val searchAreaWithCars: SearchArea = SearchArea(
    ul = GeoPoint(0, 20),
    ur = GeoPoint(20, 20),
    lr = GeoPoint(20, 0),
    ll = GeoPoint(0, 0)
  )

  def resolveAssertionWith(result: Boolean): Any => Assertion =
    _ => assert(result)

  val failedTask = Task.raiseError(new Exception)
}
