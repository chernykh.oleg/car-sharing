package utils

import akka.http.scaladsl.model.HttpHeader
import org.scalatest.matchers.{MatchResult, Matcher}

trait HeadersContainHeaderMatcher {
  class HeadersContainHeaderMatcher[H <: HttpHeader](cond: H => Boolean)
      extends Matcher[Seq[HttpHeader]] {
    override def apply(left: Seq[HttpHeader]): MatchResult = {
      MatchResult(
        left.collectFirst { case s: H if cond(s) => s }.isDefined,
        "Http headers doesn't contain header",
        "Http headers contain such header"
      )
    }
  }

  def containHeader[H <: HttpHeader](cond: H => Boolean) =
    new HeadersContainHeaderMatcher[H](cond)
}
