package utils

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.{HttpRequest, HttpResponse}
import akka.http.scaladsl.unmarshalling.{FromResponseUnmarshaller, Unmarshal}
import monix.eval.Task

case class HttpClient()(implicit system: ActorSystem) {
  def simpleSend(httpRequest: HttpRequest): Task[HttpResponse] = {
    Task.deferFuture {
      Http().singleRequest(httpRequest)
    }
  }

  def send[T: FromResponseUnmarshaller](
    httpRequest: HttpRequest
  ): Task[(HttpResponse, T)] = {
    simpleSend(httpRequest).flatMap(response => {
      Task
        .deferFutureAction { implicit scheduler =>
          Unmarshal(response).to[T]
        }
        .map(response -> _)
    })
  }
}
