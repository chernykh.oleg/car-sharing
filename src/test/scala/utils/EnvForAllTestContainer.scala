package utils

import com.dimafeng.testcontainers.DockerComposeContainer.ComposeFile
import com.dimafeng.testcontainers.{
  Container,
  DockerComposeContainer,
  ExposedService,
  ForAllTestContainer
}
import org.scalatest.Suite

import java.io.File

trait EnvForAllTestContainer extends ForAllTestContainer { self: Suite =>
  override def container: Container =
    DockerComposeContainer(
      ComposeFile(Left(new File("docker-compose-test-env.yml"))),
      exposedServices =
        Seq(ExposedService("postgres_1", 5432), ExposedService("redis_1", 6379))
    )
}
