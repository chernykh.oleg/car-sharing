import akka.actor.ActorSystem
import akka.http.scaladsl.model.headers.{Cookie, HttpCookiePair, `Set-Cookie`}
import akka.http.scaladsl.model.{
  ContentTypes,
  HttpEntity,
  HttpResponse,
  StatusCodes
}
import akka.http.scaladsl.testkit.ScalatestRouteTest
import api.UsersApi.AuthEndpoint.{AuthToken, authCookieName}
import api.UsersApi.GetCarsToBookEndpoint.SearchArea
import api.UsersApi.GetCarsToBookEndpoint
import api.UsersApi.LogInEndpoint.Credentials
import api.UsersApi.SignInEndpoint.UserRegistration
import cats.implicits.catsSyntaxApplicativeId
import io.circe.syntax.EncoderOps
import monix.eval.Task
import org.scalatest.funspec.AsyncFunSpec
import org.scalatest.matchers.should.Matchers
import utils.{EnvForAllTestContainer, HttpClient, TestEnvironment}
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport._
import org.scalatest.BeforeAndAfterAll

class AppTest
    extends AsyncFunSpec
    with EnvForAllTestContainer
    with ScalatestRouteTest
    with Matchers
    with TestEnvironment
    with BeforeAndAfterAll {

  lazy val app = Main.main(Array.empty)
  lazy val apiAddress = Main.apiServerAddressStr()
  lazy val httpClient = HttpClient()(actorSystem)

  describe("Interaction scenarios") {
    it("user should be able to make a trip") {
      app
      val resultT = for {
        _ <- signIn(validRegistrationData)
        token <- logIn(
          Credentials(
            validRegistrationData.login,
            validRegistrationData.password
          )
        )
        cars <- getAvailableCarsToBook(token, searchAreaWithCars)
        _ <- bookCar(token, cars.head.id)
        _ <- startTrip(token)
        _ <- finishTrip(token)
      } yield ()

      resultT
        .map(resolveAssertionWith(result = true))
        .onErrorHandle(resolveAssertionWith(result = false))
        .runToFuture
    }

    it("user should be able to cancel a book") {
      app
      val resultT = for {
        _ <- signIn(validRegistrationData2)
        token <- logIn(
          Credentials(
            validRegistrationData2.login,
            validRegistrationData2.password
          )
        )
        cars <- getAvailableCarsToBook(token, searchAreaWithCars)
        _ <- bookCar(token, cars.head.id)
        _ <- cancelBook(token)
        result <- hasActiveBook(token)
      } yield assert(result)

      resultT
        .onErrorHandle(resolveAssertionWith(result = false))
        .runToFuture
    }
  }

  private def signIn(registration: UserRegistration): Task[Unit] = {
    val request =
      Post(s"http://${apiAddress}/api/user/sign_in").withEntity(
        HttpEntity(
          ContentTypes.`application/json`,
          registration.asJson.toString()
        )
      )

    httpClient.simpleSend(request).flatMap {
      case HttpResponse(code, _, _, _) if code == StatusCodes.OK =>
        ().pure[Task]
      case _ => failedTask
    }
  }

  private def logIn(credentials: Credentials): Task[AuthToken] = {
    val request = Post(
      s"http://${apiAddress}/api/user/log_in?login=${credentials.login}&password=${credentials.password}"
    )

    httpClient.simpleSend(request).flatMap {
      case response @ HttpResponse(code, _, _, _) if code == StatusCodes.OK =>
        extractAuthToken(response)
          .flatMap(_.fold[Task[AuthToken]](failedTask)(identity(_).pure[Task]))
      case _ => failedTask
    }
  }

  private def getAvailableCarsToBook(
    authToken: AuthToken,
    searchArea: SearchArea
  ): Task[List[GetCarsToBookEndpoint.Output]] = {
    val request = Get(s"http://${apiAddress}/api/user/cars")
      .withHeaders(Seq(Cookie(HttpCookiePair(authCookieName, authToken.value))))
      .withEntity(
        HttpEntity(
          ContentTypes.`application/json`,
          searchArea.asJson.toString()
        )
      )

    httpClient.send[List[GetCarsToBookEndpoint.Output]](request).map {
      case (_, body) => body
    }
  }

  private def bookCar(authToken: AuthToken, carId: Long): Task[Unit] = {
    val request = Post(s"http://${apiAddress}/api/user/books?carId=${carId}")
      .withHeaders(Seq(Cookie(HttpCookiePair(authCookieName, authToken.value))))

    httpClient.simpleSend(request).flatMap {
      case HttpResponse(code, _, _, _) if code == StatusCodes.OK =>
        ().pure[Task]
      case _ => failedTask
    }
  }

  private def startTrip(authToken: AuthToken): Task[Unit] = {
    val request = Post(s"http://${apiAddress}/api/user/trips/start")
      .withHeaders(Seq(Cookie(HttpCookiePair(authCookieName, authToken.value))))

    httpClient.simpleSend(request).flatMap {
      case HttpResponse(code, _, _, _) if code == StatusCodes.OK =>
        ().pure[Task]
      case _ => failedTask
    }
  }

  private def finishTrip(authToken: AuthToken): Task[Unit] = {
    val request = Post(s"http://${apiAddress}/api/user/trips/finish")
      .withHeaders(Seq(Cookie(HttpCookiePair(authCookieName, authToken.value))))

    httpClient.simpleSend(request).flatMap {
      case HttpResponse(code, _, _, _) if code == StatusCodes.OK =>
        ().pure[Task]
      case _ => failedTask
    }
  }

  private def cancelBook(authToken: AuthToken): Task[Unit] = {
    val request = Post(s"http://${apiAddress}/api/user/books/cancel")
      .withHeaders(Seq(Cookie(HttpCookiePair(authCookieName, authToken.value))))

    httpClient.simpleSend(request).flatMap {
      case HttpResponse(code, _, _, _) if code == StatusCodes.OK =>
        ().pure[Task]
      case _ => failedTask
    }
  }

  private def hasActiveBook(authToken: AuthToken): Task[Boolean] = {
    val request = Get(s"http://${apiAddress}/api/user/books/active")
      .withHeaders(Seq(Cookie(HttpCookiePair(authCookieName, authToken.value))))

    httpClient.simpleSend(request).flatMap {
      case HttpResponse(code, _, _, _) =>
        (code == StatusCodes.NotFound).pure[Task]
    }
  }

  private def extractAuthToken(
    response: HttpResponse
  ): Task[Option[AuthToken]] = {
    response.headers
      .collectFirst {
        case header: `Set-Cookie` =>
          header.value().drop(authCookieName.length + 1).split(';').head
      }
      .map(AuthToken)
      .pure[Task]
  }
}
