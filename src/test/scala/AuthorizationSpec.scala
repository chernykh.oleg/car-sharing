import org.scalatest.funspec.AnyFunSpec
import akka.http.scaladsl.testkit.ScalatestRouteTest
import akka.http.scaladsl.server._
import akka.http.scaladsl.model.{ContentTypes, HttpEntity, StatusCodes}
import api.UsersApi.AuthEndpoint
import io.circe.syntax.EncoderOps
import org.scalatest.matchers.should.Matchers
import server.routes.UsersRoute.UsersRoute
import utils.{
  EnvForAllTestContainer,
  HeadersContainHeaderMatcher,
  TestEnvironment
}

class AuthorizationSpec
    extends AnyFunSpec
    with EnvForAllTestContainer
    with ScalatestRouteTest
    with Matchers
    with HeadersContainHeaderMatcher
    with TestEnvironment {

  lazy val testRoute: Route = {
    import Services._

    UsersRoute(
      usersService,
      authService,
      carsService,
      booksService,
      tripsService
    ).route
  }

  describe("Service Authorization") {
    it("should return 404 for log in with non-existent user") {
      Post("/api/user/log_in?login=root123&password=root123") ~> testRoute ~> check {
        status shouldBe StatusCodes.NotFound
      }
    }

    it("should successfully register new user") {
      Post("/api/user/sign_in").withEntity(
        HttpEntity(
          ContentTypes.`application/json`,
          validRegistrationData.asJson.toString()
        )
      ) ~> testRoute ~> check {
        status shouldBe StatusCodes.OK
      }
    }

    it("should return 200 and auth cookie for success log in") {
      import akka.http.scaladsl.model.headers.`Set-Cookie`
      Post(
        s"/api/user/log_in?login=${validRegistrationData.login}&password=${validRegistrationData.password}"
      ) ~> testRoute ~> check {
        status shouldBe StatusCodes.OK
        headers should containHeader[`Set-Cookie`](header => {
          val value = header.value()
          val tokenName = AuthEndpoint.authCookieName
          // +1 because of "tokenName="
          if (value.length <= tokenName.length + 1) false
          else if (!value.startsWith(tokenName)) false
          else value.drop(tokenName.length).nonEmpty
        })
      }
    }

    it("should return 400 if user with passed data is already exist") {
      Post("/api/user/sign_in").withEntity(
        HttpEntity(
          ContentTypes.`application/json`,
          validRegistrationData.asJson.toString()
        )
      ) ~> testRoute ~> check {
        status shouldBe StatusCodes.BadRequest
      }
    }

    it("should return 400 with invalid registration user data") {
      Post("/api/user/sign_in").withEntity(
        HttpEntity(
          ContentTypes.`application/json`,
          invalidRegistrationData.asJson.toString()
        )
      ) ~> testRoute ~> check {
        status shouldBe StatusCodes.BadRequest
      }
    }
  }

}
