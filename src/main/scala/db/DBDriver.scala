package db

import slick.jdbc.JdbcProfile

trait DBDriver {
  val driver: JdbcProfile
}
