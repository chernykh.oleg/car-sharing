package db

trait DBComponent extends DBDriver {

  import driver.api._

  val db: Database

}
