package db.Users

import db.{DBComponent, DBDriver}
import slick.sql.{FixedSqlAction, SqlAction}

trait UsersRepository extends UsersTable { this: DBDriver =>
  import driver.api._

  def findUserById(
    id: Long
  ): SqlAction[Option[UserModel], NoStream, Effect.Read]

  def insertUser(user: UserModel): FixedSqlAction[Long, NoStream, Effect.Write]

  def findUserByLogin(
    login: String
  ): SqlAction[Option[UserModel], NoStream, Effect.Read]
}
