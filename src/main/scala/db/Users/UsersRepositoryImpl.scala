package db.Users

import db.{DBComponent, DBDriver}
import slick.sql.{FixedSqlAction, SqlAction}

class UsersRepositoryImpl extends UsersRepository { this: DBDriver =>
  import driver.api._

  override def findUserById(
    id: Long
  ): SqlAction[Option[UserModel], NoStream, Effect.Read] =
    users.filter(_.id === id).result.headOption

  override def insertUser(
    user: UserModel
  ): FixedSqlAction[Long, NoStream, Effect.Write] =
    (users returning users.map(_.id)) += user

  override def findUserByLogin(
    login: String
  ): SqlAction[Option[UserModel], NoStream, Effect.Read] =
    users.filter(_.login === login).result.headOption
}
