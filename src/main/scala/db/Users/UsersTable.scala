package db.Users

import db.{DBComponent, DBDriver}

trait UsersTable { this: DBDriver =>
  import driver.api._

  private[db] class UsersTable(tag: Tag)
      extends Table[UserModel](tag, "users") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def login = column[String]("login", O.Unique)
    def passwordHash = column[String]("password_hash")
    def email = column[String]("email", O.Unique)
    def passportNumber = column[String]("passport_number", O.Unique)

    override def * =
      (id, login, passwordHash, email, passportNumber).mapTo[UserModel]
  }

  lazy val users = TableQuery[UsersTable]
}
