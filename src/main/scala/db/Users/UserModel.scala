package db.Users

case class UserModel(id: Long = 0L,
                     login: String,
                     passwordHash: String,
                     email: String,
                     passportNumber: String)
