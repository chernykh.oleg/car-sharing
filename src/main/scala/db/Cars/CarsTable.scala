package db.Cars

import db.DBDriver

trait CarsTable { this: DBDriver =>

  import driver.api._

  class CarTable(tag: Tag) extends Table[CarModel](tag, "cars") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def vin = column[String]("vin", O.Unique)
    def number = column[String]("number", O.Unique)
    def brand = column[String]("brand")
    def model = column[String]("model")
    def availableToUse = column[Boolean]("available_to_use")

    override def * =
      (id, vin, number, brand, model, availableToUse).mapTo[CarModel]
  }

  lazy val cars = TableQuery[CarTable]
}
