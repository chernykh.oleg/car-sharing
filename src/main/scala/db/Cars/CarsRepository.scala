package db.Cars

import db.{DBComponent, DBDriver}
import slick.dbio.Effect._
import slick.sql.{FixedSqlStreamingAction, SqlAction}

trait CarsRepository extends CarsTable {
  this: DBDriver =>

  import driver.api._

  def findAvailableCarsToBookByIds(
    carIds: List[Long]
  ): FixedSqlStreamingAction[Seq[CarModel], CarModel, Read]

  def findCarById(
    carId: Long,
    blockReturned: Boolean = false
  ): SqlAction[Option[CarModel], NoStream, Read]

  def findCarByVIN(vin: String): SqlAction[Option[CarModel], NoStream, Read]

  def updateCar(car: CarModel): SqlAction[Int, NoStream, Write]
}
