package db.Cars

case class CarModel(id: Long = 0L,
                    vin: String,
                    number: String,
                    brand: String,
                    model: String,
                    availableToUse: Boolean)
