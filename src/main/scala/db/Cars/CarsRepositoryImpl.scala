package db.Cars

import db.{DBComponent, DBDriver}
import slick.dbio.Effect.{Read, Write}
import slick.dbio.NoStream
import slick.sql.{FixedSqlStreamingAction, SqlAction}

class CarsRepositoryImpl extends CarsRepository { this: DBDriver =>
  import driver.api._

  override def findAvailableCarsToBookByIds(
    carIds: List[Long]
  ): FixedSqlStreamingAction[Seq[CarModel], CarModel, Read] =
    cars.filter(c => c.id.inSet(carIds) && c.availableToUse).result

  override def findCarById(
    carId: Long,
    blockReturned: Boolean = false
  ): SqlAction[Option[CarModel], NoStream, Read] = {
    val baseQuery = cars.filter(_.id === carId)
    val query = if (blockReturned) baseQuery.forUpdate else baseQuery
    query.result.headOption
  }

  override def findCarByVIN(
    vin: String
  ): SqlAction[Option[CarModel], NoStream, Read] =
    cars.filter(_.vin === vin).result.headOption

  override def updateCar(car: CarModel): SqlAction[Int, NoStream, Write] =
    cars.filter(_.id === car.id).update(car)
}
