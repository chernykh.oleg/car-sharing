package db.Trips

import db.{DBComponent, DBDriver}
import slick.dbio.Effect._
import slick.dbio.{Effect, NoStream}
import slick.sql.{FixedSqlAction, SqlAction}

trait TripsRepository extends TripsTable {
  this: DBDriver =>

  def findTripById(
    id: Long
  ): SqlAction[Option[TripModel], NoStream, Effect.Read]

  def findTripByCarId(
    carId: Long
  ): SqlAction[Option[TripModel], NoStream, Effect.Read]

  def findActiveTripByUserId(
    userId: Long
  ): SqlAction[Option[TripModel], NoStream, Effect.Read]

  def findTripByUserId(
    userId: Long
  ): SqlAction[Option[TripModel], NoStream, Effect.Read]

  def insertTrip(trip: TripModel): FixedSqlAction[Long, NoStream, Effect.Write]

  def insertOrUpdateTrip(trip: TripModel): FixedSqlAction[Int, NoStream, Write]

}
