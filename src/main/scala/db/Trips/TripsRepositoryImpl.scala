package db.Trips

import db.{DBComponent, DBDriver}
import slick.dbio.Effect.Write
import slick.sql.{FixedSqlAction, SqlAction}

class TripsRepositoryImpl extends TripsRepository { this: DBDriver =>
  import driver.api._

  override def findTripById(
    id: Long
  ): SqlAction[Option[TripModel], NoStream, Effect.Read] =
    trips.filter(_.id === id).forUpdate.result.headOption

  override def findTripByCarId(
    carId: Long
  ): SqlAction[Option[TripModel], NoStream, Effect.Read] =
    trips.filter(_.carId === carId).forUpdate.result.headOption

  override def findTripByUserId(
    userId: Long
  ): SqlAction[Option[TripModel], NoStream, Effect.Read] =
    trips.filter(_.userId === userId).forUpdate.result.headOption

  def findActiveTripByUserId(
    userId: Long
  ): SqlAction[Option[TripModel], NoStream, Effect.Read] =
    trips
      .filter(_.userId === userId)
      .sortBy(_.finishAt.nullsFirst)
      .result
      .headOption

  override def insertTrip(
    trip: TripModel
  ): FixedSqlAction[Long, NoStream, Effect.Write] =
    (trips returning trips.map(_.id)) += trip

  override def insertOrUpdateTrip(
    trip: TripModel
  ): FixedSqlAction[Int, NoStream, Write] = trips.insertOrUpdate(trip)
}
