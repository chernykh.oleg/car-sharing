package db.Trips

case class TripModel(id: Long = 0L,
                     userId: Long,
                     carId: Long,
                     costPerMinute: Double = 10,
                     startAt: Long,
                     finishAt: Option[Long])
