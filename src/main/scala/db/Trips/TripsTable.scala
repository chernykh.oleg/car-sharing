package db.Trips

import db.Cars.CarsTable
import db.{DBComponent, DBDriver}
import db.Users.UsersTable

import java.sql.Date

trait TripsTable extends UsersTable with CarsTable { this: DBDriver =>

  import driver.api._

  class TripsTable(tag: Tag) extends Table[TripModel](tag, "trips") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def userId = column[Long]("user_id")
    def carId = column[Long]("car_id")
    def costPerMinute = column[Double]("cost_per_minute")
    def startAt = column[Long]("start_at")
    def finishAt = column[Long]("finish_at")
    def user = foreignKey("user_fk", userId, users)(_.id)
    def car = foreignKey("car_fk", carId, cars)(_.id)
    override def * =
      (id, userId, carId, costPerMinute, startAt, finishAt.?).mapTo[TripModel]
  }

  lazy val trips = TableQuery[TripsTable]
}
