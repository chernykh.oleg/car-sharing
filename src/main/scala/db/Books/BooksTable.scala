package db.Books

import db.Cars.CarsTable
import db.{DBComponent, DBDriver}
import db.Users.UsersTable
import slick.dbio.Effect
import slick.sql.FixedSqlAction

import java.sql.Date

trait BooksTable extends UsersTable with CarsTable { this: DBDriver =>
  import driver.api._

  class BooksTable(tag: Tag) extends Table[BookModel](tag, "books") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def userId = column[Long]("user_id")
    def carId = column[Long]("car_id")
    def openedAt = column[Long]("opened_at")
    def duration = column[Long]("book_duration")
    def costPerMinute = column[Double]("cost_per_minute")
    def user = foreignKey("user_fk", userId, users)(_.id)
    def car = foreignKey("car_fk", carId, cars)(_.id)
    override def * =
      (id, userId, carId, openedAt, duration, costPerMinute).mapTo[BookModel]
  }

  val books = TableQuery[BooksTable]
}
