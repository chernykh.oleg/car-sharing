package db.Books

import db.{DBComponent, DBDriver}
import slick.dbio.Effect.{Read, Write}
import slick.dbio.NoStream
import slick.sql.{FixedSqlAction, FixedSqlStreamingAction, SqlAction}
import utils.DateUtils.{dateOps, durationOps}

import java.util.Date
import scala.concurrent.duration.FiniteDuration

class BooksRepositoryImpl extends BooksRepository { this: DBDriver =>

  import driver.api._

  override def findBookById(
    bookId: Long
  ): SqlAction[Option[BookModel], NoStream, Effect.Read] =
    books.filter(_.id === bookId).result.headOption

  override def findBookByCarId(
    id: Long
  ): SqlAction[Option[BookModel], NoStream, Effect.Read] =
    books.filter(_.carId === id).result.headOption

  override def findBooksExpiredIn(
    interval: FiniteDuration
  ): FixedSqlStreamingAction[Seq[BookModel], BookModel, Read] = {
    val currentTime: Long = new Date().toDBTimeRepr
    books
      .filter(
        table =>
          (table.openedAt + table.duration - currentTime) < interval.toDBTimeRepr
      )
      .result
  }

  override def findBookByUserId(
    id: Long
  ): SqlAction[Option[BookModel], NoStream, Effect.Read] =
    books.filter(_.userId === id).result.headOption

  override def insertBook(
    book: BookModel
  ): FixedSqlAction[Long, NoStream, Effect.Write] =
    books.returning(books.map(_.id)) += book

  override def deleteBookById(
    bookId: Long
  ): FixedSqlAction[Int, NoStream, Write] =
    books.filter(_.id === bookId).delete

  override def deleteBookByUserId(
    userId: Long
  ): FixedSqlAction[Int, NoStream, Write] =
    books.filter(_.userId === userId).delete

}
