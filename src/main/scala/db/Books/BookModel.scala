package db.Books

case class BookModel(id: Long = 0L,
                     userId: Long,
                     carId: Long,
                     openedAt: Long,
                     duration: Long,
                     costPerMinute: Double)
