package db.Books

import db.DBDriver
import slick.dbio.{Effect, NoStream}
import slick.dbio.Effect._
import slick.sql.{FixedSqlAction, FixedSqlStreamingAction, SqlAction}

import scala.concurrent.duration.FiniteDuration

trait BooksRepository extends BooksTable { this: DBDriver =>

  def findBookById(
    bookId: Long
  ): SqlAction[Option[BookModel], NoStream, Effect.Read]

  def findBookByCarId(
    id: Long
  ): SqlAction[Option[BookModel], NoStream, Effect.Read]

  def findBooksExpiredIn(
    interval: FiniteDuration
  ): FixedSqlStreamingAction[Seq[BookModel], BookModel, Read]

  def findBookByUserId(
    id: Long
  ): SqlAction[Option[BookModel], NoStream, Effect.Read]

  def insertBook(book: BookModel): FixedSqlAction[Long, NoStream, Effect.Write]

  def deleteBookById(bookId: Long): FixedSqlAction[Int, NoStream, Write]

  def deleteBookByUserId(userId: Long): FixedSqlAction[Int, NoStream, Write]

}
