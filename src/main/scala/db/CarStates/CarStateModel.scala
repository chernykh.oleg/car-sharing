package db.CarStates

case class CarStateModel(id: Long = 0L,
                         carId: Long,
                         fuel: Double,
                         isLock: Boolean,
                         speed: Int,
                         latitude: Double,
                         longitude: Double,
                         generatedAt: Long)
