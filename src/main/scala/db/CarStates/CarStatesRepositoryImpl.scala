package db.CarStates

import api.UsersApi.GetCarsToBookEndpoint.SearchArea
import db.{DBComponent, DBDriver}
import slick.dbio.Effect.{Read, Write}
import slick.jdbc.{GetResult, PositionedResult}
import slick.sql.{FixedSqlAction, FixedSqlStreamingAction, SqlStreamingAction}

object CarStatesRepositoryImpl {
  implicit val getResult: GetResult[CarStateModel] =
    new GetResult[CarStateModel] {
      override def apply(r: PositionedResult): CarStateModel =
        CarStateModel(
          id = r.<<,
          carId = r.<<,
          fuel = r.<<,
          isLock = r.<<,
          speed = r.<<,
          latitude = r.<<,
          longitude = r.<<,
          generatedAt = r.<<
        )
    }
}

class CarStatesRepositoryImpl extends CarStatesRepository { this: DBDriver =>
  import driver.api._
  import CarStatesRepositoryImpl._

  def findLastStatesByCarIds(
    carIds: List[Long]
  ): SqlStreamingAction[Vector[CarStateModel], CarStateModel, Effect.Read] = {
    sql"""
          SELECT "id", "car_id", "fuel", "is_lock", "speed", "latitude", "longitude", max("generated_at") 
          FROM "car_states" 
          WHERE "car_id" in (${carIds.mkString(",")}) 
          GROUP BY "id", "car_id", "fuel", "is_lock", "speed", "latitude", "longitude" 
       """
      .as[CarStateModel]
  }

  def findStatesOfCarsInArea(
    searchArea: SearchArea
  ): SqlStreamingAction[Vector[CarStateModel], CarStateModel, Effect.Read] =
    sql"""
          SELECT t.*
          FROM (
            SELECT *, row_number() over (partition by "car_id" order by "generated_at" desc) as row
            FROM "car_states"
          ) as t 
          WHERE 
            (t.row = 1) AND 
            (t.latitude BETWEEN ${searchArea.ul.latitude} AND ${searchArea.ur.latitude}) AND
            (t.longitude BETWEEN ${searchArea.ll.longitude} AND ${searchArea.ul.longitude})
       """
      .as[CarStateModel]

  def insertStates(
    batch: Seq[CarStateModel]
  ): FixedSqlAction[Option[Int], NoStream, Write] = {

    /**
      * ++= gives you an accumulated count in an Option
      * (which can be None if the database system does not provide counts for all rows)
      */
    carStates ++= batch
  }

  def findStatesInInterval(
    carId: Long,
    timeFrom: Long,
    timeTo: Long
  ): FixedSqlStreamingAction[Seq[CarStateModel], CarStateModel, Read] =
    carStates
      .filter(
        table =>
          (table.generatedAt >= timeFrom) && (table.generatedAt <= timeTo) && (table.carId === carId)
      )
      .sortBy(_.generatedAt.asc)
      .result
}
