package db.CarStates

import api.UsersApi.GetCarsToBookEndpoint.SearchArea
import db.{DBComponent, DBDriver}
import slick.dbio.{Effect, NoStream}
import slick.dbio.Effect._
import slick.jdbc.{GetResult, PositionedResult}
import slick.sql.{
  FixedSqlAction,
  FixedSqlStreamingAction,
  SqlAction,
  SqlStreamingAction
}

import java.sql.Date

trait CarStatesRepository extends CarStatesTable { this: DBDriver =>

  def findLastStatesByCarIds(
    carIds: List[Long]
  ): SqlStreamingAction[Vector[CarStateModel], CarStateModel, Effect.Read]

  def findStatesOfCarsInArea(
    searchArea: SearchArea
  ): SqlStreamingAction[Vector[CarStateModel], CarStateModel, Effect.Read]

  def insertStates(
    batch: Seq[CarStateModel]
  ): FixedSqlAction[Option[Int], NoStream, Write]

  def findStatesInInterval(
    carId: Long,
    timeFrom: Long,
    timeTo: Long
  ): FixedSqlStreamingAction[Seq[CarStateModel], CarStateModel, Read]
}
