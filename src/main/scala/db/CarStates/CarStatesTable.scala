package db.CarStates

import db.{DBComponent, DBDriver}

import java.sql.Date

trait CarStatesTable { this: DBDriver =>

  import driver.api._

  class CarStatesTable(tag: Tag)
      extends Table[CarStateModel](tag, "car_states") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def carId = column[Long]("car_id")
    def fuel = column[Double]("fuel")
    def isLock = column[Boolean]("is_lock")
    def speed = column[Int]("speed")
    def latitude = column[Double]("latitude")
    def longitude = column[Double]("longitude")
    def generatedAt = column[Long]("generated_at")
    override def * =
      (id, carId, fuel, isLock, speed, latitude, longitude, generatedAt)
        .mapTo[CarStateModel]
  }

  lazy val carStates = TableQuery[CarStatesTable]
}
