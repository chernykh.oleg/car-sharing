package server.routes

import akka.http.scaladsl.server.Route
import sttp.tapir.Endpoint
import sttp.tapir.docs.openapi.OpenAPIDocsInterpreter
import sttp.tapir.openapi.circe.yaml._
import sttp.tapir.swagger.akkahttp.SwaggerAkka

object ApiDescriptionRoute {
  def route(routes: Seq[Endpoint[_, _, _, _]]*): Route =
    new SwaggerAkka(
      OpenAPIDocsInterpreter
        .toOpenAPI(routes.flatten, "Car sharing", "1.0")
        .toYaml
    ).routes
}
