package server.routes.UsersRoute

import akka.http.scaladsl.server.Route
import api.UsersApi.UsersApi
import cats.implicits.{catsSyntaxApplicativeId, catsSyntaxEitherId}
import logic.AuthService.AuthService
import logic.BooksService.{BookServiceError, BooksService}
import logic.BooksService.BooksService.Book
import monix.eval.Task
import monix.execution.Scheduler
import sttp.model.StatusCode
import sttp.tapir.server.akkahttp.AkkaHttpServerInterpreter
import utils.TypeMapper
import io.scalaland.chimney.dsl.TransformerOps
import utils.TypeMapper.TypeMapperOps

import scala.concurrent.Future
import api.UsersApi.GetActiveBookEndpoint._
import sttp.model.StatusCode.NotFound
import utils.DateUtils.dateOps

object GetActiveBookRoute {
  implicit val mapper: TypeMapper[Book, Output] =
    TypeMapper.instance[Book, Output](
      _.into[Output]
        .withFieldComputed(_.bookId, _.id)
        .withFieldComputed(
          _.willClosedAt,
          b => b.openedAt.addDuration(b.duration)
        )
        .transform
    )
}

case class GetActiveBookRoute(
  booksService: BooksService,
  authService: AuthService
)(implicit scheduler: Scheduler) {
  import GetActiveBookRoute._

  def route: Route =
    AkkaHttpServerInterpreter.toRoute(UsersApi.getActiveBook)(routeLogic)

  private def routeLogic(input: Input): Future[Either[StatusCode, Output]] = {
    authService
      .extract(input) { user =>
        booksService
          .findBookByUserId(user.id)
          .map(_.filter(_.userId == user.id))
          .map { bookOpt =>
            bookOpt.fold(NotFound.asLeft[Output])(
              _.mapTo[Output].asRight[StatusCode]
            )
          }
      }
      .runToFuture
  }

}
