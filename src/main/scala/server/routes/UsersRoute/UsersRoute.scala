package server.routes.UsersRoute

import akka.http.scaladsl.server.{Route, RouteConcatenation}
import logic.AuthService.AuthService
import logic.BooksService.BooksService
import logic.UsersService.UsersService
import monix.execution.Scheduler
import logic.CarsService.CarsService
import logic.TripsService.TripsService

case class UsersRoute(
  usersService: UsersService,
  authService: AuthService,
  carsService: CarsService,
  booksService: BooksService,
  tripsService: TripsService
)(implicit scheduler: Scheduler) {
  lazy val route: Route = RouteConcatenation.concat(
    LogInRoute(authService).route,
    SignInRoute(usersService).route,
    GetCarsToBookRoute(authService, carsService).route,
    BookCarRoute(booksService, authService).route,
    GetActiveBookRoute(booksService, authService).route,
    CancelBookRoute(authService, booksService).route,
    StartTripRoute(authService, tripsService).route,
    GetActiveTripRoute(authService, tripsService).route,
    FinishTripRoute(authService, tripsService).route
  )
}
