package server.routes.UsersRoute

import akka.http.scaladsl.server.Route
import api.UsersApi.UsersApi
import sttp.tapir.server.akkahttp.AkkaHttpServerInterpreter
import api.UsersApi.BookCarEndpoint._
import cats.implicits.catsSyntaxEitherId
import io.scalaland.chimney.dsl.TransformerOps
import logic.AuthService.AuthService
import logic.BooksService.{BookServiceError, BooksService}
import logic.BooksService.BooksService.Book
import logic.TripsService.TripsService
import monix.execution.Scheduler
import sttp.model.StatusCode
import sttp.model.StatusCode._
import utils.DateUtils.dateOps
import utils.TypeMapper
import utils.TypeMapper.TypeMapperOps
import BookServiceError._
import akka.http.scaladsl.model.StatusCodes

import scala.concurrent.Future

object BookCarRoute {
  implicit val outputMapper: TypeMapper[Book, Output] =
    TypeMapper.instance[Book, Output](
      _.into[Output]
        .withFieldComputed(_.bookId, _.id)
        .withFieldComputed(
          _.willClosedAt,
          b => b.openedAt.addDuration(b.duration)
        )
        .transform
    )
}

case class BookCarRoute(booksService: BooksService, authService: AuthService)(
  implicit scheduler: Scheduler
) {
  import BookCarRoute._

  def route: Route =
    AkkaHttpServerInterpreter.toRoute(UsersApi.bookCar)(routeLogic)

  private def routeLogic(input: Input): Future[Either[StatusCode, Output]] =
    authService
      .extract(input) { user =>
        booksService.bookCar(user.id, input.carId).map { result =>
          result.fold(mapToEndpointError, _.mapTo[Output].asRight)
        }
      }
      .runToFuture

  private def mapToEndpointError(
    error: BookServiceError
  ): Either[StatusCode, Output] =
    error match {
      case CarIsNotAvailableToUse => BadRequest.asLeft
      case BookCarError(_)        => BadRequest.asLeft
      case _                      => InternalServerError.asLeft
    }
}
