package server.routes.UsersRoute

import akka.http.scaladsl.server.Route
import api.UsersApi.LogInEndpoint.Credentials
import api.UsersApi.{AuthEndpoint, UsersApi}
import cats.implicits.catsSyntaxEitherId
import com.typesafe.scalalogging.LazyLogging
import logic.AuthService.AuthServiceError.UserNotFound
import logic.AuthService.{AuthService, AuthServiceError}
import monix.execution.Scheduler
import sttp.model.StatusCode
import sttp.model.headers.CookieValueWithMeta
import sttp.tapir.server.akkahttp.AkkaHttpServerInterpreter

import scala.concurrent.Future

case class LogInRoute(authService: AuthService)(implicit scheduler: Scheduler) {
  def route: Route =
    AkkaHttpServerInterpreter.toRoute(UsersApi.logIn)(routeLogic)

  private def routeLogic(
    credentials: Credentials
  ): Future[Either[StatusCode, CookieValueWithMeta]] = {
    authService
      .authorize(credentials)
      .map { result =>
        result.fold(mapToEndpointError, token => {
          AuthEndpoint
            .makeAuthCookie(token.value)
            .asRight
        })
      }
      .runToFuture
  }

  private def mapToEndpointError(
    error: AuthServiceError
  ): Either[StatusCode, Nothing] =
    error match {
      case UserNotFound => StatusCode.NotFound.asLeft
    }
}
