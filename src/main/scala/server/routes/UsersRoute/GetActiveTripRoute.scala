package server.routes.UsersRoute

import akka.http.scaladsl.server.Route
import api.UsersApi.UsersApi
import monix.execution.Scheduler
import sttp.model.StatusCode
import api.UsersApi.GetActiveTripEndpoint.Output
import logic.AuthService.AuthService
import sttp.tapir.server.akkahttp.AkkaHttpServerInterpreter

import scala.concurrent.Future
import api.UsersApi.GetActiveTripEndpoint._
import cats.data.OptionT
import cats.implicits.catsSyntaxEitherId
import io.scalaland.chimney.dsl.TransformerOps
import logic.TripsService.TripsService
import logic.TripsService.TripsService.TripResult
import sttp.model.StatusCode.{InternalServerError, NotFound}
import utils.TypeMapper
import utils.TypeMapper.TypeMapperOps

object GetActiveTripRoute {
  implicit val mapper: TypeMapper[TripResult, Output] =
    TypeMapper.instance[TripResult, Output](
      _.into[Output]
        .withFieldComputed(_.currentDistance, _.totalDistance)
        .withFieldComputed(_.currentCost, _.totalCost)
        .transform
    )
}

case class GetActiveTripRoute(
  authService: AuthService,
  tripsService: TripsService
)(implicit scheduler: Scheduler) {

  import GetActiveTripRoute._

  def route: Route =
    AkkaHttpServerInterpreter.toRoute(UsersApi.getActiveTrip)(routeLogic)

  private def routeLogic(input: Input): Future[Either[StatusCode, Output]] = {
    authService
      .extract(input) { user =>
        tripsService.findActiveTripByUserId(user.id).map { tripOpt =>
          tripOpt.fold[Either[StatusCode, Output]](NotFound.asLeft)(
            _.mapTo[Output].asRight
          )
        }
      }
      .runToFuture
  }
}
