package server.routes.UsersRoute

import akka.http.scaladsl.server.Route
import api.UsersApi.UsersApi
import logic.AuthService.AuthService
import logic.TripsService.{TripServiceError, TripsService}
import sttp.tapir.server.akkahttp.AkkaHttpServerInterpreter
import api.UsersApi.StartTripEndpoint._
import cats.implicits.catsSyntaxEitherId
import monix.execution.Scheduler
import sttp.model.StatusCode
import sttp.model.StatusCode._
import TripServiceError._

import scala.concurrent.Future

case class StartTripRoute(authService: AuthService, tripsService: TripsService)(
  implicit scheduler: Scheduler
) {
  def route: Route =
    AkkaHttpServerInterpreter.toRoute(UsersApi.startTrip)(routeLogic)

  private def routeLogic(input: Input): Future[Either[StatusCode, Output]] =
    authService
      .extract(input) { user =>
        tripsService
          .startTrip(user.id)
          .map(_.fold(mapToEndpointError, Output(_).asRight))
      }
      .runToFuture

  private def mapToEndpointError(
    error: TripServiceError
  ): Either[StatusCode, Nothing] = error match {
    case BookNotFound => NotFound.asLeft
    case _            => InternalServerError.asLeft
  }
}
