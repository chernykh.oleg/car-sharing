package server.routes.UsersRoute

import akka.http.scaladsl.server.Route
import api.UsersApi.UsersApi
import logic.AuthService.AuthService
import logic.TripsService.{TripServiceError, TripsService}
import sttp.tapir.server.akkahttp.AkkaHttpServerInterpreter
import api.UsersApi.FinishTripEndpoint.{Input, Output}
import cats.implicits.catsSyntaxEitherId
import io.scalaland.chimney.dsl.TransformerOps
import logic.TripsService.TripsService.TripResult
import monix.execution.Scheduler
import sttp.model.StatusCode
import utils.TypeMapper
import utils.TypeMapper.TypeMapperOps
import TripServiceError._
import StatusCode._

import scala.concurrent.Future

object FinishTripRoute {
  implicit val outputMapper: TypeMapper[TripResult, Output] =
    TypeMapper.instance[TripResult, Output](_.into[Output].transform)
}

case class FinishTripRoute(authService: AuthService, tripsService: TripsService)(
  implicit scheduler: Scheduler
) {
  import FinishTripRoute._

  def route: Route =
    AkkaHttpServerInterpreter.toRoute(UsersApi.finishTrip)(routeLogic)

  private def routeLogic(input: Input): Future[Either[StatusCode, Output]] =
    authService
      .extract(input) { user =>
        tripsService
          .finishTrip(user.id)
          .map(_.fold(mapToEndpointError, _.mapTo[Output].asRight))
      }
      .runToFuture

  private def mapToEndpointError(
    error: TripServiceError
  ): Either[StatusCode, Nothing] = error match {
    case TripNotFound => NotFound.asLeft
    case _            => InternalServerError.asLeft
  }
}
