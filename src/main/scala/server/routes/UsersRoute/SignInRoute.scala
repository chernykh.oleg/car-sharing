package server.routes.UsersRoute

import akka.http.scaladsl.server.Route
import api.UsersApi.SignInEndpoint.{
  NoContent,
  SignInEndpointError,
  UserAlreadyExist => UserAlreadyExistEndpointError
}
import api.UsersApi.UsersApi
import cats.implicits.catsSyntaxEitherId
import logic.UsersService.{UsersService, UsersServiceError}
import monix.execution.Scheduler
import sttp.model.StatusCode
import sttp.tapir.server.akkahttp.AkkaHttpServerInterpreter
import UsersServiceError._
import api.UsersApi.SignInEndpoint.UserRegistration

import scala.concurrent.Future

case class SignInRoute(usersService: UsersService)(
  implicit scheduler: Scheduler
) {

  def route: Route =
    AkkaHttpServerInterpreter.toRoute(UsersApi.signIn)(routeLogic)

  def routeLogic(
    registration: UserRegistration
  ): Future[Either[SignInEndpointError, StatusCode]] =
    usersService
      .registerUser(registration)
      .map[Either[SignInEndpointError, StatusCode]] { result =>
        result.fold(mapToEndpointError, _ => StatusCode.Ok.asRight)
      }
      .runToFuture

  private def mapToEndpointError(
    error: UsersServiceError
  ): Either[SignInEndpointError, Nothing] = error match {
    case Validation(err)      => err.asLeft
    case UserAlreadyExist     => UserAlreadyExistEndpointError.asLeft
    case RegistrationError(_) => NoContent.asLeft
  }
}
