package server.routes.UsersRoute

import akka.http.scaladsl.server.Route
import api.UsersApi.{CancelBookEndpoint, UsersApi}
import logic.AuthService.AuthService
import logic.BooksService.{BookServiceError, BooksService}
import monix.execution.Scheduler
import sttp.tapir.server.akkahttp.AkkaHttpServerInterpreter
import api.UsersApi.CancelBookEndpoint._
import cats.implicits.catsSyntaxEitherId
import sttp.model.StatusCode
import logic.BooksService.BookServiceError._

import scala.concurrent.Future

case class CancelBookRoute(authService: AuthService, booksService: BooksService)(
  implicit scheduler: Scheduler
) {
  def route: Route =
    AkkaHttpServerInterpreter.toRoute(UsersApi.cancelBook)(routeLogic)

  def routeLogic(input: Input): Future[Either[StatusCode, StatusCode]] =
    authService
      .extract(input) { user =>
        booksService
          .closeBookByUserId(user.id)
          .map(_.fold(mapToEndpointError, _ => StatusCode.Ok.asRight))
      }
      .runToFuture

  def mapToEndpointError(error: BookServiceError): Either[StatusCode, Nothing] =
    error match {
      case BookNotFound => StatusCode.NotFound.asLeft
      case _            => StatusCode.InternalServerError.asLeft
    }
}
