package server.routes.UsersRoute

import akka.http.scaladsl.server.Route
import api.UsersApi.GetCarsToBookEndpoint.Output
import api.UsersApi.UsersApi
import cats.implicits.catsSyntaxEitherId
import io.scalaland.chimney.dsl.TransformerOps
import logic.AuthService.AuthService
import logic.CarsService.CarsService
import logic.CarsService.CarsService.{Car, CarWithState}
import monix.execution.Scheduler
import sttp.model.StatusCode
import sttp.tapir.server.akkahttp.AkkaHttpServerInterpreter
import utils.TypeMapper
import utils.TypeMapper.TypeMapperOps

object GetCarsToBookRoute {
  implicit val mapper: TypeMapper[CarWithState, Output] =
    TypeMapper.instance[CarWithState, Output](_.into[Output].transform)
}

case class GetCarsToBookRoute(
  authService: AuthService,
  carsService: CarsService
)(implicit scheduler: Scheduler) {
  import GetCarsToBookRoute._

  def route: Route =
    AkkaHttpServerInterpreter.toRoute(UsersApi.getCarsToBook) { input =>
      authService
        .extract(input) { _ =>
          carsService
            .findCarsToBook(input.searchArea)
            .map(cars => cars.map(_.mapTo[Output]).asRight)
        }
        .runToFuture
    }
}
