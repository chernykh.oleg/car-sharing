package server.routes.CarsRoute

import akka.http.scaladsl.server.Route
import logic.CarsService.CarsService
import monix.execution.Scheduler

case class CarsRoute(carsService: CarsService)(implicit scheduler: Scheduler) {
  lazy val route: Route = GetCarDescriptionRoute(carsService).route
}
