package server.routes.CarsRoute

import akka.http.scaladsl.server.Route
import api.CarsApi.CarsApi
import api.CarsApi.GetCarDescriptionEndpoint.CarDescription
import cats.implicits.catsSyntaxEitherId
import com.typesafe.scalalogging.LazyLogging
import io.scalaland.chimney.dsl.TransformerOps
import logic.CarsService.CarsService
import logic.CarsService.CarsService.Car
import monix.execution.Scheduler
import sttp.model.StatusCode
import sttp.model.StatusCode._
import sttp.tapir.server.akkahttp.AkkaHttpServerInterpreter
import utils.TypeMapper
import utils.TypeMapper.TypeMapperOps

object GetCarDescriptionRoute {
  implicit val mapper: TypeMapper[Car, CarDescription] =
    TypeMapper.instance[Car, CarDescription](
      _.into[CarDescription].withFieldComputed(_.carId, _.id).transform
    )
}

case class GetCarDescriptionRoute(carsService: CarsService)(
  implicit scheduler: Scheduler
) extends LazyLogging {

  import GetCarDescriptionRoute._

  def route: Route =
    AkkaHttpServerInterpreter.toRoute(CarsApi.getCarDescription) { vin =>
      carsService
        .findCarByVIN(vin)
        .map { carDescriptionOpt =>
          carDescriptionOpt.fold(NotFound.asLeft[CarDescription])(
            _.mapTo[CarDescription].asRight[StatusCode]
          )
        }
        .runToFuture
    }
}
