package server

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.StatusCode
import akka.http.scaladsl.server.Directives.complete
import akka.http.scaladsl.server.{ExceptionHandler, Route, RouteConcatenation}
import api.CarsApi.CarsApi
import api.UsersApi.UsersApi
import com.typesafe.scalalogging.LazyLogging
import monix.execution.Scheduler
import logic.AuthService.AuthService
import logic.BooksService.BooksService
import logic.CarsService.CarsService
import logic.TripsService.TripsService
import logic.UsersService.UsersService
import server.AppConfig.ApiConfig
import server.routes.CarsRoute.CarsRoute
import server.routes.ApiDescriptionRoute
import server.routes.UsersRoute.UsersRoute
import akka.http.scaladsl.model.StatusCodes
import monix.eval.Task

class AppServer(api: ApiConfig,
                usersService: UsersService,
                authService: AuthService,
                carsService: CarsService,
                booksService: BooksService,
                tripsService: TripsService)(implicit actorSystem: ActorSystem)
    extends LazyLogging {

  private implicit val scheduler: Scheduler = Scheduler(actorSystem.dispatcher)

  val appRoute: Route =
    Route.seal(
      RouteConcatenation.concat(
        ApiDescriptionRoute.route(UsersApi.endpoints, CarsApi.endpoints),
        UsersRoute(
          usersService,
          authService,
          carsService,
          booksService,
          tripsService
        ).route,
        CarsRoute(carsService).route
      )
    )(exceptionHandler = ExceptionHandler {
      case err => {
        logger.error(err.getMessage)
        complete(StatusCodes.InternalServerError)
      }
    })

  def start(): Task[Unit] =
    Task.deferFuture {
      Http().newServerAt(api.host, api.port).bind(appRoute)
    }.void
}
