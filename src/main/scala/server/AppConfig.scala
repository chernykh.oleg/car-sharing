package server

import server.AppConfig.{
  ApiConfig,
  BookConfig,
  CarNodeConfig,
  CarStatesConsumerConfig,
  SessionStorage
}

import scala.concurrent.duration.FiniteDuration

object AppConfig {
  case class ApiConfig(host: String, port: Int)
  case class SessionStorage(host: String, port: Int)
  case class BookConfig(duration: FiniteDuration,
                        cancellerPeriod: FiniteDuration,
                        costPerMinute: Int)

  case class CarNodeKafkaConfig(topic: String,
                                bootstrapServer: String,
                                schemaUrl: String)
  case class CarNodeConfig(sensorPollPeriod: FiniteDuration,
                           kafka: CarNodeKafkaConfig)

  case class CarStatesConsumerConfig(topic: String,
                                     bootstrapServer: String,
                                     consumerGroup: String,
                                     schemaUrl: String)
}

case class AppConfig(api: ApiConfig,
                     sessionStorage: SessionStorage,
                     book: BookConfig,
                     carNode: CarNodeConfig,
                     carStatesConsumer: CarStatesConsumerConfig) {
  val databaseConfigPath: String = "database"
  lazy val apiAddressStr = s"${api.host}:${api.port}"
}
