import com.typesafe.scalalogging.LazyLogging
import logic.BooksService.BooksService
import logic.BooksService.BooksService.Book
import monix.eval.Task
import monix.execution.Scheduler
import monix.execution.Cancelable

import scala.concurrent.duration.{Duration, FiniteDuration}

case class BookCanceller(period: FiniteDuration, booksService: BooksService)(
  implicit scheduler: Scheduler
) extends LazyLogging {

  def start(): Task[Unit] = {
    Task.eval {
      scheduler.scheduleWithFixedDelay(Duration.Zero, period) {
        booksService
          .findBooksExpiredIn(period)
          .foreachL(scheduleBooksForClosing)
          .runToFuture
      }
    }
  }

  private def closeBookWithDelay(userId: Long, delay: FiniteDuration): Unit =
    scheduler.scheduleOnce(delay) {
      closeBook(userId).runToFuture
    }

  private def closeBook(userId: Long): Task[Unit] =
    booksService
      .closeBookByUserId(userId)
      .map(_.fold(e => logger.error(e.getMessage), identity))

  private def scheduleBooksForClosing(books: List[Book]): Unit = {
    books.foreach(book => {
      val timeToClose = book.timeToClose()
      if (timeToClose.compare(Duration.Zero) <= 0)
        closeBook(book.userId).runToFuture
      else closeBookWithDelay(book.userId, timeToClose)
    })
  }
}
