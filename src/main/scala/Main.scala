import akka.actor.ActorSystem
import monix.eval.Task
import monix.execution.Scheduler
import server.AppServer
import statestreaming.CarNode.CarNode
import statestreaming.CarStatesConsumer
import utils.CommonUtils.actorSystemOps

object Main extends App {

  import Services._

  def apiServerAddressStr(): String = config.apiAddressStr

  val actorSystem: ActorSystem = ActorSystem()

  implicit val ec: Scheduler = actorSystem.monixScheduler()

  val apiServer = new AppServer(
    config.api,
    usersService,
    authService,
    carsService,
    booksService,
    tripsService
  )(actorSystem)

  val bookCanceller = BookCanceller(config.book.cancellerPeriod, booksService)

  // Каждая машина знает свой vin
  val vins = List("zxsaqwe1", "zxsaqwe2", "zxsaqwe3", "zxsaqwe4", "zxsaqwe5")
  val carNodes = vins.map(vin => {
    CarNode(
      vin,
      config.carNode,
      api,
      carStatesStorage,
      doorSensor,
      fuelSensor,
      speedSensor,
      geoPositionSensor
    )
  })

  val carStatesConsumer =
    CarStatesConsumer(config.carStatesConsumer, carStatesService)

  val launchAppT = for {
    _ <- apiServer.start()
    _ <- bookCanceller.start()
    _ <- Task.traverse(carNodes)(_.start())
    _ <- carStatesConsumer.start()
  } yield ()

  launchAppT.runToFuture
}
