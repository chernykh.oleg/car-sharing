import akka.actor.ActorSystem
import db.Books.BooksRepositoryImpl
import db.CarStates.CarStatesRepositoryImpl
import db.Cars.CarsRepositoryImpl
import db.DBComponent
import db.Trips.TripsRepositoryImpl
import db.Users.UsersRepositoryImpl
import logic.AuthService.AuthService.AuthorizedUser
import logic.AuthService.AuthServiceImpl
import logic.AuthService.AuthTokenGenerator.AuthTokenGeneratorImpl
import logic.BooksService.BooksServiceImpl
import logic.CarsService.CarsServiceImpl
import logic.TripsService.TripsServiceImpl
import logic.UsersService.UsersServiceImpl
import logic.UsersService.Validator.ValidatorImpl
import monix.eval.Task
import pureconfig.ConfigSource
import server.AppConfig
import utils.PasswordHashing.Bcrypt
import pureconfig._
import pureconfig.generic.auto._
import statestreaming.CarNode.API.APILocal
import statestreaming.CarNode.CarStatesStorage.CarStatesStorageImpl
import statestreaming.CarNode.Sensors.DoorStateSensor.DoorStateSensorImpl
import statestreaming.CarNode.Sensors.FuelSensor.FuelSensorImpl
import statestreaming.CarNode.Sensors.GeoPositionSensor.GeoPositionSensorImpl
import statestreaming.CarNode.Sensors.SpeedSensor.SpeedSensorImpl

object Services {
  val config: AppConfig = ConfigSource.default.loadOrThrow[AppConfig]

  import scalacache._
  import scalacache.redis._
  import scalacache.serialization.circe.codec

  private implicit val mode: Mode[Task] = scalacache.CatsEffect.modes.async
  private val sessionCache: Cache[AuthorizedUser] =
    RedisCache(config.sessionStorage.host, config.sessionStorage.port)

  private val dbInstance = slick.jdbc.PostgresProfile.backend.Database
    .forConfig(config.databaseConfigPath)

  trait DBComponentImpl extends DBComponent {
    override val driver = slick.jdbc.PostgresProfile
    override val db: driver.api.Database = dbInstance
  }

  private val usersRepository = new UsersRepositoryImpl with DBComponentImpl
  private val carsRepository = new CarsRepositoryImpl with DBComponentImpl
  private val booksRepository = new BooksRepositoryImpl with DBComponentImpl
  private val carStatesRepository = new CarStatesRepositoryImpl
  with DBComponentImpl
  private val tripsRepository = new TripsRepositoryImpl with DBComponentImpl

  /**
    * Dependencies for api
    */
  val bcrypt = new Bcrypt()
  val usersService =
    new UsersServiceImpl(bcrypt, usersRepository, ValidatorImpl)
    with DBComponentImpl
  val tokenGenerator = new AuthTokenGeneratorImpl
  val authService =
    new AuthServiceImpl(usersService, bcrypt, sessionCache, tokenGenerator)
  val carsService = new CarsServiceImpl(carsRepository, carStatesRepository)
  with DBComponentImpl
  val booksService =
    new BooksServiceImpl(config.book, booksRepository, carsRepository)
    with DBComponentImpl
  val tripsService =
    new TripsServiceImpl(
      tripsRepository,
      booksRepository,
      carsRepository,
      carStatesRepository
    ) with DBComponentImpl

  /**
    * Dependencies for car node
    */
  val api = APILocal(carsService)
  val carStatesStorage = CarStatesStorageImpl(config.carNode.kafka)
  val doorSensor = DoorStateSensorImpl()
  val fuelSensor = FuelSensorImpl()
  val speedSensor = SpeedSensorImpl()
  val geoPositionSensor = GeoPositionSensorImpl()

  /**
    * Dependency for car state consumer
    */
  import logic.CarStatesService.{
    CarStatesServiceImpl => ApiCarStatesServiceImpl
  }
  val carStatesService = new ApiCarStatesServiceImpl(carStatesRepository)
  with DBComponentImpl
}
