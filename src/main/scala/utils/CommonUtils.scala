package utils

import akka.actor.ActorSystem
import monix.execution.Scheduler

object CommonUtils {
  implicit class eitherOps[E, T](private val v: Either[E, Option[T]])
      extends AnyVal {
    def toRightOpt: Option[T] = v.fold(_ => None, identity)
  }

  implicit class actorSystemOps(private val actorSystem: ActorSystem)
      extends AnyVal {
    def monixScheduler(): Scheduler = Scheduler(actorSystem.dispatcher)
  }
}
