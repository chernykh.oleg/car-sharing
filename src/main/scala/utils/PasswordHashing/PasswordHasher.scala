package utils.PasswordHashing

import monix.eval.Task

trait PasswordHasher {
  def hash(password: String, rounds: Int = 12): Task[String]
}
