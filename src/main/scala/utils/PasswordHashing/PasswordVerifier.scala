package utils.PasswordHashing

import monix.eval.Task

trait PasswordVerifier {
  def verify(password: String, hash: String): Task[Boolean]
}
