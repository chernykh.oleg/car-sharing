package utils.PasswordHashing

import cats.implicits.catsSyntaxApplicativeId
import monix.eval.Task
import com.github.t3hnar.bcrypt.BCryptStrOps

class Bcrypt extends PasswordHasher with PasswordVerifier {
  override def hash(password: String, rounds: Int): Task[String] =
    password.bcryptBounded(rounds).pure[Task]

  override def verify(password: String, hash: String): Task[Boolean] =
    password.isBcryptedBounded(hash).pure[Task]
}
