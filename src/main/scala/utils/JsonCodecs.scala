package utils

import io.circe.{Decoder, Encoder}

import java.text.SimpleDateFormat
import java.util.Date
import scala.util.Try

object JsonCodecs {
  private val dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss")
  implicit val dateEncoder: Encoder[Date] =
    Encoder.encodeString.contramap[Date](_.toString)
  implicit val dateDecoder: Decoder[Date] =
    Decoder.decodeString.emapTry(str => Try(dateFormat.parse(str)))
}
