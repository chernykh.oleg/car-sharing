package utils

import java.util.Date
import java.util.concurrent.TimeUnit
import scala.concurrent.duration.FiniteDuration

object DateUtils {
  implicit class dateOps(private val date: Date) extends AnyVal {
    def toDBTimeRepr: Long = date.getTime
    def addDuration(duration: FiniteDuration) =
      new Date(date.getTime + duration.toMillis)
  }

  implicit class durationOps(private val duration: FiniteDuration)
      extends AnyVal {
    def toDBTimeRepr: Long = duration.toMillis
  }

  implicit class longOps(private val time: Long) {
    def toDate = new Date(time)

    def toFiniteDuration: FiniteDuration =
      FiniteDuration(time, TimeUnit.MILLISECONDS)
  }
}
