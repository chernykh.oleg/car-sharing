package utils

trait TypeMapper[From, To] {
  def mapTo(from: From): To
}

object TypeMapper {
  implicit class TypeMapperOps[From](private val v: From) extends AnyVal {
    def mapTo[To](implicit mapper: TypeMapper[From, To]): To = mapper.mapTo(v)
  }

  def instance[From, To](map: From => To): TypeMapper[From, To] =
    new TypeMapper[From, To] {
      override def mapTo(from: From): To = map(from)
    }
}
