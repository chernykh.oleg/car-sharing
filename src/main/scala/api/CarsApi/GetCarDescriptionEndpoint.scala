package api.CarsApi

import io.circe.generic.JsonCodec
import sttp.model.StatusCode
import sttp.model.StatusCode._
import sttp.tapir._
import sttp.tapir.generic.auto.schemaForCaseClass
import sttp.tapir.json.circe.jsonBody

object GetCarDescriptionEndpoint {
  import api.Api.RootEndpoint

  @JsonCodec
  case class CarDescription(carId: Long)

  def apply(
    rootEndpoint: RootEndpoint
  ): Endpoint[String, StatusCode, CarDescription, Any] =
    rootEndpoint.get
      .summary("Описание автомобиля")
      .in(
        "description" / path[String](name = "vin")
          .description("VIN номер автомобиля")
      )
      .out(jsonBody[CarDescription].description("Описание автомобиля"))
      .errorOut(
        statusCode
          .description(NotFound, "Автомобиль не найден")
          .description(InternalServerError, "Внутренняя ошибка сервера")
      )
}
