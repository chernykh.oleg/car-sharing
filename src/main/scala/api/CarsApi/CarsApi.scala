package api.CarsApi

import sttp.tapir._

object CarsApi {
  type RootEndpoint = Endpoint[Unit, Unit, Unit, Any]

  private val rootEndpoint = endpoint.in("api" / "cars")

  val getCarDescription = GetCarDescriptionEndpoint(rootEndpoint)

  val endpoints: Seq[Endpoint[_, _, _, _]] =
    Seq(getCarDescription)
}
