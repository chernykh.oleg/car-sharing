package api.UsersApi

import api.Api.RootEndpoint
import io.circe.generic.JsonCodec
import sttp.model.StatusCode
import sttp.tapir._
import sttp.tapir.generic.auto._
import sttp.tapir.json.circe.jsonBody

object SignInEndpoint {

  @JsonCodec
  case class UserRegistration(login: String,
                              password: String,
                              email: String,
                              passportNumber: String)

  sealed trait SignInEndpointError

  object UserAlreadyExist extends SignInEndpointError

  @JsonCodec
  case class ValidationError(login: Option[String] = None,
                             password: Option[String] = None,
                             email: Option[String] = None,
                             passportNumber: Option[String] = None)
      extends SignInEndpointError

  object NoContent extends SignInEndpointError

  def apply(
    rootEndpoint: RootEndpoint
  ): Endpoint[UserRegistration, SignInEndpointError, StatusCode, Any] =
    rootEndpoint.post
      .summary("Регистрация")
      .in("sign_in")
      .in(jsonBody[UserRegistration])
      .out(
        statusCode.description(StatusCode.Ok, "Успешное создание пользователя")
      )
      .errorOut(
        oneOf[SignInEndpointError](
          oneOfMapping(
            StatusCode.BadRequest,
            jsonBody[ValidationError].description("Валидация переданных полей")
          ),
          oneOfMapping(StatusCode.BadRequest, emptyOutputAs(UserAlreadyExist)),
          oneOfMapping(StatusCode.InternalServerError, emptyOutputAs(NoContent))
        )
      )
}
