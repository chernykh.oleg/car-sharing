package api.UsersApi

import sttp.tapir._
import api.Api.RootEndpoint
import sttp.model.StatusCode
import sttp.model.headers.CookieValueWithMeta

object UsersApi {
  private val rootEndpoint: RootEndpoint = endpoint.in("api" / "user")
  private val authorizedEndpoint = AuthEndpoint(rootEndpoint)
  private val carsRootEndpoint = authorizedEndpoint.in("cars")
  private val bookCarRootEndpoint = authorizedEndpoint.in("books")
  private val tripRootEndpoint = authorizedEndpoint.in("trips")

  val logIn = LogInEndpoint(rootEndpoint)

  val signIn = SignInEndpoint(rootEndpoint)

  val getCarsToBook = GetCarsToBookEndpoint(carsRootEndpoint)

  val bookCar = BookCarEndpoint(bookCarRootEndpoint)

  val getActiveBook = GetActiveBookEndpoint(bookCarRootEndpoint)

  val startTrip = StartTripEndpoint(tripRootEndpoint)

  val finishTrip = FinishTripEndpoint(tripRootEndpoint)

  val getActiveTrip = GetActiveTripEndpoint(tripRootEndpoint)

  val cancelBook = CancelBookEndpoint(bookCarRootEndpoint)

  val endpoints: Seq[Endpoint[_, _, _, _]] =
    Seq(
      logIn,
      signIn,
      getCarsToBook,
      bookCar,
      getActiveBook,
      startTrip,
      finishTrip,
      getActiveTrip,
      cancelBook
    )
}
