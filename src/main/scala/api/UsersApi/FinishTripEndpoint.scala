package api.UsersApi

import api.UsersApi.AuthEndpoint.{AuthToken, AuthorizedEndpoint, HasAuthToken}
import io.circe.generic.JsonCodec
import sttp.model.StatusCode
import sttp.tapir.generic.auto.schemaForCaseClass
import sttp.tapir.json.circe.jsonBody
import sttp.tapir._

object FinishTripEndpoint {
  @JsonCodec
  case class Output(tripId: Long,
                    totalDistance: Double,
                    tripDurationInMinutes: Int,
                    costPerMinute: Double,
                    totalCost: Double)

  case class Input(authToken: AuthToken) extends HasAuthToken

  def apply(
    authorizedEndpoint: AuthorizedEndpoint
  ): Endpoint[Input, StatusCode, Output, Any] =
    authorizedEndpoint.post
      .summary("Завершает текущую поездку")
      .in("finish")
      .mapInTo(Input)
      .out(jsonBody[Output].description("Результат поездки"))
      .errorOut(
        statusCode
          .description(StatusCode.Unauthorized, "Пользователь не авторизован")
          .description(StatusCode.NotFound, "Активная поездка не найдена")
          .description(
            StatusCode.InternalServerError,
            "Внутренняя ошибка сервера"
          )
      )
}
