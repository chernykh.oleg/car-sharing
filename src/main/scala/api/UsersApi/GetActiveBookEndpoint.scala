package api.UsersApi

import api.UsersApi.AuthEndpoint.{AuthToken, AuthorizedEndpoint, HasAuthToken}
import io.circe.generic.JsonCodec
import sttp.model.StatusCode
import sttp.tapir._
import sttp.tapir.generic.auto.schemaForCaseClass
import sttp.tapir.json.circe.jsonBody

import java.util.Date

object GetActiveBookEndpoint {
  import utils.JsonCodecs._

  @JsonCodec
  case class Output(bookId: Long,
                    carId: Long,
                    openedAt: Date,
                    willClosedAt: Date)

  case class Input(authToken: AuthToken) extends HasAuthToken

  def apply(
    authorizedEndpoint: AuthorizedEndpoint
  ): Endpoint[Input, StatusCode, Output, Any] =
    authorizedEndpoint.get
      .summary("Активная бронь пользователя")
      .in("active")
      .mapInTo(Input)
      .out(jsonBody[Output].description("Описание брони"))
      .errorOut(
        statusCode
          .description(StatusCode.Unauthorized, "Пользователь не авторизован")
          .description(StatusCode.NotFound, "Активная бронь не найдена")
          .description(
            StatusCode.InternalServerError,
            "Внутренняя ошибка сервера"
          )
      )
}
