package api.UsersApi

import api.UsersApi.AuthEndpoint.{AuthToken, AuthorizedEndpoint, HasAuthToken}
import sttp.model.StatusCode
import sttp.tapir._

object CancelBookEndpoint {
  case class Input(authToken: AuthToken) extends HasAuthToken

  def apply(
    authorizedEndpoint: AuthorizedEndpoint
  ): Endpoint[Input, StatusCode, StatusCode, Any] =
    authorizedEndpoint.post
      .summary("Отменяет бронь пользователя")
      .in("cancel")
      .mapInTo(Input)
      .out(statusCode.description(StatusCode.Ok, "Бронь успешно отменена"))
      .errorOut(
        statusCode
          .description(StatusCode.Unauthorized, "Пользователь не авторизован")
          .description(StatusCode.NotFound, "Активная бронь не найдена")
          .description(
            StatusCode.InternalServerError,
            "Внутрення ошибка сервера"
          )
      )
}
