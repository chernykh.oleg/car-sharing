package api.UsersApi

import api.UsersApi.AuthEndpoint.{AuthToken, AuthorizedEndpoint, HasAuthToken}
import io.circe.generic.JsonCodec
import sttp.model.StatusCode
import sttp.tapir._
import sttp.tapir.generic.auto.schemaForCaseClass
import sttp.tapir.json.circe.jsonBody

import java.util.Date

object BookCarEndpoint {
  import utils.JsonCodecs._

  @JsonCodec
  case class Output(bookId: Long, willClosedAt: Date)

  case class Input(authToken: AuthToken, carId: Long) extends HasAuthToken

  def apply(
    authorizedEndpoint: AuthorizedEndpoint
  ): Endpoint[Input, StatusCode, Output, Any] =
    authorizedEndpoint.post
      .summary("Бронирует машину для пользователя")
      .in(
        query[Long]("carId")
          .description("Идентификатор автомобиля для бронирования")
      )
      .mapInTo(Input)
      .out(jsonBody[Output].description("Идентификатор открытой брони"))
      .errorOut(
        statusCode
          .description(StatusCode.Unauthorized, "Пользователь не авторизован")
          .description(StatusCode.BadRequest, "Невозможно создать бронь")
          .description(
            StatusCode.InternalServerError,
            "Внутренняя ошибка сервера"
          )
      )
}
