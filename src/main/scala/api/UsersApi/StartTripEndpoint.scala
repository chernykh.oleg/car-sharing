package api.UsersApi

import api.UsersApi.AuthEndpoint.{AuthToken, AuthorizedEndpoint, HasAuthToken}
import io.circe.generic.JsonCodec
import sttp.model.StatusCode
import sttp.tapir.generic.auto.schemaForCaseClass
import sttp.tapir.json.circe.jsonBody
import sttp.tapir.{Endpoint, query, statusCode}

object StartTripEndpoint {
  @JsonCodec
  case class Output(id: Long)

  case class Input(authToken: AuthToken) extends HasAuthToken

  def apply(
    authorizedEndpoint: AuthorizedEndpoint
  ): Endpoint[Input, StatusCode, Output, Any] =
    authorizedEndpoint.post
      .summary("Начать поездку пользователя")
      .in("start")
      .mapInTo(Input)
      .out(jsonBody[Output].description("Идентификатор поездки"))
      .errorOut(
        statusCode
          .description(StatusCode.Unauthorized, "Пользователь не авторизован")
          .description(StatusCode.NotFound, "Активная бронь не найдена")
          .description(
            StatusCode.InternalServerError,
            "Внутрення ошибка сервера"
          )
      )
}
