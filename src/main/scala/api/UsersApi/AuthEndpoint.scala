package api.UsersApi

import api.Api.RootEndpoint
import api.UsersApi.AuthEndpoint.AuthToken
import cats.implicits.catsSyntaxOptionId
import sttp.model.headers.{CookieValueWithMeta, CookieWithMeta}
import sttp.tapir.{Endpoint, cookie, endpoint}

object AuthEndpoint {
  type AuthorizedEndpoint = Endpoint[AuthToken, Unit, Unit, Any]

  val authCookieName: String = "token"

  def makeAuthCookie(value: String): CookieValueWithMeta =
    CookieValueWithMeta(
      value = value,
      expires = None,
      maxAge = None,
      domain = None,
      path = "/".some,
      secure = false,
      httpOnly = false,
      sameSite = None,
      otherDirectives = Map.empty
    )

  case class AuthToken(value: String)

  trait HasAuthToken {
    val authToken: AuthToken
  }

  def apply(rootEndpoint: RootEndpoint): AuthorizedEndpoint =
    rootEndpoint
      .in(cookie[String](authCookieName).description("Токен авторизации"))
      .mapInTo(AuthToken)
}
