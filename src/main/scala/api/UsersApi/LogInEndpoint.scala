package api.UsersApi

import api.Api.RootEndpoint
import sttp.model.StatusCode
import sttp.model.headers.CookieValueWithMeta
import sttp.tapir._
import sttp.tapir.generic.auto.schemaForCaseClass
import sttp.tapir.json.circe.jsonBody

object LogInEndpoint {
  case class Credentials(login: String, password: String)

  val credentials: EndpointInput[Credentials] =
    query[String](name = "login")
      .and(query[String](name = "password"))
      .mapTo(Credentials)

  def apply(
    rootEndpoint: RootEndpoint
  ): Endpoint[Credentials, StatusCode, CookieValueWithMeta, Any] =
    rootEndpoint.post
      .summary("Авторизация")
      .in("log_in")
      .in(credentials)
      .out(
        statusCode(StatusCode.Ok)
          .description("Успешный вход в сервис")
          .and(setCookie("token").description("Токен авторизации"))
      )
      .errorOut(
        statusCode
          .description(
            StatusCode.NotFound,
            "Пользователь с такими данными не найден"
          )
          .description(
            StatusCode.InternalServerError,
            "Внутренняя ошибка сервера"
          )
      )
}
