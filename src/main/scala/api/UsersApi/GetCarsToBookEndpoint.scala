package api.UsersApi

import api.UsersApi.AuthEndpoint.{AuthToken, AuthorizedEndpoint, HasAuthToken}
import io.circe.generic.JsonCodec
import sttp.model.StatusCode
import sttp.tapir.generic.auto.schemaForCaseClass
import sttp.tapir.json.circe.jsonBody
import sttp.tapir._

object GetCarsToBookEndpoint {
  @JsonCodec
  case class Output(id: Long,
                    brand: String,
                    model: String,
                    number: String,
                    fuel: Double,
                    geoPoint: GeoPoint)
  @JsonCodec
  case class GeoPoint(latitude: Double, longitude: Double)
  @JsonCodec
  case class SearchArea(ul: GeoPoint, ur: GeoPoint, lr: GeoPoint, ll: GeoPoint)

  case class Input(authToken: AuthToken, searchArea: SearchArea)
      extends HasAuthToken

  def apply(
    authorizedEndpoint: AuthorizedEndpoint
  ): Endpoint[Input, StatusCode, List[Output], Any] =
    authorizedEndpoint.get
      .summary("Доступные машины для бронирования в определенной области")
      .in(
        jsonBody[SearchArea].description("Область поиска доступных автомобилей")
      )
      .mapInTo(Input)
      .out(
        jsonBody[List[Output]].description(
          "Список автомобилей доступных для бронирования в переданной площади"
        )
      )
      .errorOut(
        statusCode
          .description(StatusCode.Unauthorized, "Пользователь не авторизован")
          .description(
            StatusCode.InternalServerError,
            "Внутренняя ошибка сервера"
          )
      )
}
