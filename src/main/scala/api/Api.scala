package api

import sttp.tapir.Endpoint

object Api {
  type RootEndpoint = Endpoint[Unit, Unit, Unit, Any]
}
