package statestreaming

import api.UsersApi.GetCarsToBookEndpoint.GeoPoint
import cats.implicits.catsSyntaxApplicativeId
import com.typesafe.scalalogging.LazyLogging
import protobuf.State
import io.confluent.kafka.serializers.protobuf.KafkaProtobufDeserializerConfig
import logic.CarStatesService.CarStatesService
import logic.CarStatesService.CarStatesService.CarState
import monix.eval.Task
import monix.execution.Scheduler
import org.apache.kafka.clients.consumer.{ConsumerConfig, KafkaConsumer}
import server.AppConfig.CarStatesConsumerConfig
import statestreaming.CarStatesConsumer.protobufStateOps
import utils.DateUtils.longOps

import java.time.{Duration => JavaDuration}
import java.util.{Collections, Properties}
import scala.concurrent.duration.{DurationInt, FiniteDuration}
import scala.jdk.CollectionConverters.IterableHasAsScala

object CarStatesConsumer {
  implicit class protobufStateOps(private val state: State) extends AnyVal {
    def toCarState: CarState = CarState(
      carId = state.getCarId,
      fuel = state.getFuel,
      isLock = state.getIsLock,
      speed = state.getSpeed,
      geoPoint = GeoPoint(
        latitude = state.getLocation.getLatitude,
        longitude = state.getLocation.getLongitude
      ),
      generatedAt = state.getGeneratedAt.toDate
    )
  }
}

case class CarStatesConsumer(
  config: CarStatesConsumerConfig,
  carStatesService: CarStatesService
)(implicit scheduler: Scheduler)
    extends LazyLogging {

  private lazy val properties = {

    val properties = new Properties();

    properties.put(
      ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG,
      config.bootstrapServer
    )

    properties.put(ConsumerConfig.GROUP_ID_CONFIG, config.consumerGroup)

    properties.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest")

    properties.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, false)

    properties.put(
      ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG,
      "org.apache.kafka.common.serialization.StringDeserializer"
    )

    properties.put(
      ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG,
      "io.confluent.kafka.serializers.protobuf.KafkaProtobufDeserializer"
    )

    properties.put("schema.registry.url", config.schemaUrl)

    properties.put(
      KafkaProtobufDeserializerConfig.SPECIFIC_PROTOBUF_VALUE_TYPE,
      State.getDescriptor.getFullName
    )

    properties
  }

  private lazy val consumer = new KafkaConsumer[String, State](properties)

  def start(): Task[Unit] = Task.eval {
    consumer.subscribe(Collections.singleton(config.topic))
    fetchBatchOfStates(delay = 5 seconds)
  }

  def fetchBatchOfStates(delay: FiniteDuration): Unit = {
    scheduler.scheduleOnce(delay) {
      val records = consumer.poll(JavaDuration.ofMillis(500))
      val carStates = records.asScala.toSeq.map(_.value().toCarState)
      carStatesService
        .insertCarStates(carStates)
        .foreachL(_ => {
          consumer.commitAsync()
          fetchBatchOfStates(delay)
        })
        .runToFuture
    }
  }
}
