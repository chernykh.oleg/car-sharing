package statestreaming.CarNode.Sensors

import monix.eval.Task

trait Sensor[T] {
  def poll(): Task[T]
}
