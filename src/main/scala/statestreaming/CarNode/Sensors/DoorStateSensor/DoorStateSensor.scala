package statestreaming.CarNode.Sensors.DoorStateSensor

import statestreaming.CarNode.Sensors.Sensor

trait DoorStateSensor extends Sensor[Boolean]
