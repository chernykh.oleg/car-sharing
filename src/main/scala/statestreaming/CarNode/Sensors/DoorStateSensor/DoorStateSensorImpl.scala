package statestreaming.CarNode.Sensors.DoorStateSensor

import cats.implicits.catsSyntaxApplicativeId
import monix.eval.Task

import scala.util.Random

case class DoorStateSensorImpl() extends DoorStateSensor {
  override def poll(): Task[Boolean] = Random.nextBoolean().pure[Task]
}
