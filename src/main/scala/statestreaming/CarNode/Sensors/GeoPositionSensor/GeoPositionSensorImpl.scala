package statestreaming.CarNode.Sensors.GeoPositionSensor

import cats.implicits.catsSyntaxApplicativeId
import monix.eval.Task
import statestreaming.CarNode.Sensors.GeoPositionSensor.GeoPositionSensor.GeoPoint

import scala.util.Random

case class GeoPositionSensorImpl() extends GeoPositionSensor {
  private val generator = Random
  private def makeRandomCoordinate(): Double = Math.abs(generator.nextDouble())

  override def poll(): Task[GeoPoint] =
    GeoPoint(
      latitude = makeRandomCoordinate(),
      longitude = makeRandomCoordinate()
    ).pure[Task]
}
