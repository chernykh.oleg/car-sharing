package statestreaming.CarNode.Sensors.GeoPositionSensor

import statestreaming.CarNode.Sensors.GeoPositionSensor.GeoPositionSensor.GeoPoint
import statestreaming.CarNode.Sensors.Sensor

object GeoPositionSensor {
  case class GeoPoint(latitude: Double, longitude: Double)
}

trait GeoPositionSensor extends Sensor[GeoPoint]
