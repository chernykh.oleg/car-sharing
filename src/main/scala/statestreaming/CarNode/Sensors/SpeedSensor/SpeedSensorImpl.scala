package statestreaming.CarNode.Sensors.SpeedSensor

import cats.implicits.catsSyntaxApplicativeId
import monix.eval.Task

import scala.util.Random

case class SpeedSensorImpl() extends SpeedSensor {
  override def poll(): Task[Int] = Math.abs(Random.nextInt(100)).pure[Task]
}
