package statestreaming.CarNode.Sensors.SpeedSensor

import statestreaming.CarNode.Sensors.Sensor

trait SpeedSensor extends Sensor[Int]
