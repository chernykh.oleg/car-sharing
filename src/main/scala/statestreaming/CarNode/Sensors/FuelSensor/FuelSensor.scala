package statestreaming.CarNode.Sensors.FuelSensor

import statestreaming.CarNode.Sensors.Sensor

trait FuelSensor extends Sensor[Double]
