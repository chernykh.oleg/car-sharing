package statestreaming.CarNode.Sensors.FuelSensor

import cats.implicits.catsSyntaxApplicativeId
import monix.eval.Task

import scala.util.Random

case class FuelSensorImpl() extends FuelSensor {
  override def poll(): Task[Double] = Math.abs(Random.nextDouble()).pure[Task]
}
