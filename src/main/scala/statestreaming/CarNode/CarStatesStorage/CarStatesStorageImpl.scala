package statestreaming.CarNode.CarStatesStorage

import com.typesafe.scalalogging.LazyLogging
import monix.eval.Task
import org.apache.kafka.clients.producer.{
  KafkaProducer,
  ProducerConfig,
  ProducerRecord
}
import protobuf.State
import server.AppConfig.{CarNodeConfig, CarNodeKafkaConfig}

import java.util.Properties
import scala.concurrent.Future

case class CarStatesStorageImpl(config: CarNodeKafkaConfig)
    extends CarStatesStorage
    with LazyLogging {

  private lazy val properties: Properties = {
    val properties = new Properties();
    properties.put(
      ProducerConfig.BOOTSTRAP_SERVERS_CONFIG,
      config.bootstrapServer
    );

    properties.put(
      ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG,
      "org.apache.kafka.common.serialization.StringSerializer"
    )

    properties.put(
      ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,
      "io.confluent.kafka.serializers.protobuf.KafkaProtobufSerializer"
    )

    properties.put("schema.registry.url", config.schemaUrl)

    properties
  }

  private lazy val producer = new KafkaProducer[String, State](properties)

  override def push(state: State): Task[Unit] =
    Task
      .deferFutureAction { implicit scheduler =>
        Future {
          val record =
            new ProducerRecord[String, State](config.topic, state)
          // TODO: rewrite to non-blocking
          producer.send(record).get()
        }
      }
      .void
      .onErrorHandle(err => {
        logger.error(err.toString)
      })

}
