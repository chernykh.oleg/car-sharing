package statestreaming.CarNode.CarStatesStorage

import monix.eval.Task
import protobuf.State

trait CarStatesStorage {
  def push(state: State): Task[Unit]
}
