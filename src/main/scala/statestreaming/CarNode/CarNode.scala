package statestreaming.CarNode

import statestreaming.CarNode.API.API
import statestreaming.CarNode.CarStatesStorage.CarStatesStorage
import monix.eval.Task
import monix.execution.Scheduler
import statestreaming.CarNode.Sensors.FuelSensor.FuelSensor
import statestreaming.CarNode.Sensors.SpeedSensor.SpeedSensor
import statestreaming.CarNode.Sensors.DoorStateSensor.DoorStateSensor
import statestreaming.CarNode.Sensors.GeoPositionSensor.GeoPositionSensor
import com.typesafe.scalalogging.LazyLogging
import protobuf.{Location, State}
import server.AppConfig.CarNodeConfig

import java.util.Date
import scala.concurrent.duration.Duration

case class CarNode(
  vin: String,
  config: CarNodeConfig,
  api: API,
  carStatesStorage: CarStatesStorage,
  doorStateSensor: DoorStateSensor,
  fuelSensor: FuelSensor,
  speedSensor: SpeedSensor,
  geoPositionSensor: GeoPositionSensor
)(implicit scheduler: Scheduler)
    extends LazyLogging {

  private def gatherState(): Task[State.Builder] = {
    for {
      isLock <- doorStateSensor.poll()
      fuel <- fuelSensor.poll()
      speed <- speedSensor.poll()
      geoPosition <- geoPositionSensor.poll()
    } yield
      State
        .newBuilder()
        .setFuel(fuel)
        .setIsLock(isLock)
        .setSpeed(speed)
        .setLocation(
          Location
            .newBuilder()
            .setLatitude(geoPosition.latitude)
            .setLongitude(geoPosition.longitude)
            .build()
        )
        .setGeneratedAt(new Date().getTime)
  }

  def start(): Task[Unit] =
    api
      .findCarIdByVIN(vin)
      .flatMap { carIdOpt =>
        carIdOpt.fold[Task[Long]](
          Task.raiseError(
            new Exception("Автомобиль не зарегистрирован в системе")
          )
        )(Task.pure)
      }
      .map { carId =>
        scheduler.scheduleWithFixedDelay(Duration.Zero, config.sensorPollPeriod) {
          gatherState()
            .map(_.setCarId(carId.toInt).build())
            .flatMap(carStatesStorage.push)
            .onErrorHandle(e => logger.error(e.toString))
            .runToFuture
        }
      }
      .onErrorHandle(e => logger.error(e.toString))
      .void
}
