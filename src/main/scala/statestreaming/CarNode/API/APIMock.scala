package statestreaming.CarNode.API
import cats.implicits.{catsSyntaxApplicativeId, catsSyntaxOptionId}
import monix.eval.Task

case class APIMock() extends API {
  override def findCarIdByVIN(vin: String): Task[Option[Long]] =
    1L.some.pure[Task]
}
