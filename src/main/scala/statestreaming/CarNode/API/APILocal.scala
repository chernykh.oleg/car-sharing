package statestreaming.CarNode.API
import cats.data.OptionT
import logic.CarsService.CarsService
import monix.eval.Task

case class APILocal(carsService: CarsService) extends API {
  override def findCarIdByVIN(vin: String): Task[Option[Long]] =
    OptionT {
      carsService.findCarByVIN(vin)
    }.map(_.id).value
}
