package statestreaming.CarNode.API

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import monix.eval.Task
import server.AppConfig.ApiConfig
import akka.http.scaladsl.client.RequestBuilding.Get
import akka.http.scaladsl.model.ContentTypes
import akka.http.scaladsl.model.StatusCodes.{OK, UnsupportedMediaType}
import akka.http.scaladsl.unmarshalling.Unmarshal
import cats.implicits.{catsSyntaxApplicativeId, catsSyntaxOptionId}
import com.typesafe.scalalogging.LazyLogging
import io.circe.Decoder
import io.circe.generic.semiauto.deriveDecoder
import io.circe.parser.decode

import scala.concurrent.ExecutionContextExecutor

object APIImpl {
  case class CarInfo(id: Long)
  val carInfoDecoder: Decoder[CarInfo] = deriveDecoder[CarInfo]
}

case class APIImpl(config: ApiConfig)(implicit actorSystem: ActorSystem)
    extends API
    with LazyLogging {

  import APIImpl._

  private implicit val dispatcher: ExecutionContextExecutor =
    actorSystem.dispatcher

  override def findCarIdByVIN(vin: String): Task[Option[Long]] = {
    val request = Get(
      s"http://${config.host}:${config.port}/cars/description/$vin"
    )

    Task
      .deferFuture {
        Http().singleRequest(request)
      }
      .flatMap { response =>
        response.status match {
          case OK
              if response.entity.contentType == ContentTypes.`application/json` =>
            Task
              .deferFuture {
                Unmarshal(response.entity).to[String]
              }
              .map(responseStr => decode(responseStr)(carInfoDecoder))
              .map(_.fold(e => { logException(e); None }, _.id.some))
          case _ => {
            logException(new Exception("Неожиданная ошибка")); None.pure[Task]
          }
        }
      }
  }

  private def logException(e: Exception): Unit =
    logger.error(e.toString).pure[Task]
}
