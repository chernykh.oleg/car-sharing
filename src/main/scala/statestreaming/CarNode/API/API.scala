package statestreaming.CarNode.API

import monix.eval.Task

trait API {
  def findCarIdByVIN(vin: String): Task[Option[Long]]
}
