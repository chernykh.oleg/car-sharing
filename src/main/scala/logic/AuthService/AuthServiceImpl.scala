package logic.AuthService

import api.UsersApi.AuthEndpoint.AuthToken
import api.UsersApi.LogInEndpoint
import cats.implicits.{catsSyntaxApplicativeId, catsSyntaxEitherId}
import io.scalaland.chimney.dsl.TransformerOps
import logic.AuthService.AuthService.AuthorizedUser
import logic.AuthService.AuthServiceError.UserNotFound
import logic.AuthService.AuthTokenGenerator.AuthTokenGenerator
import logic.UsersService.UsersService
import logic.UsersService.UsersService.User
import monix.eval.Task
import scalacache.{Cache, Mode}
import utils.PasswordHashing.PasswordVerifier
import utils.TypeMapper
import utils.TypeMapper.TypeMapperOps

object AuthServiceImpl {
  implicit val mapper: TypeMapper[User, AuthorizedUser] =
    TypeMapper.instance[User, AuthorizedUser](_.into[AuthorizedUser].transform)
}

class AuthServiceImpl(
  usersService: UsersService,
  verifier: PasswordVerifier,
  sessionCache: Cache[AuthorizedUser],
  tokenGenerator: AuthTokenGenerator
)(implicit val mode: Mode[Task])
    extends AuthService {
  import AuthServiceImpl._

  override def authorize(
    credentials: LogInEndpoint.Credentials
  ): Task[Either[AuthServiceError, AuthToken]] =
    usersService
      .findUserByLogin(credentials.login)
      .flatMap { userOpt =>
        userOpt.fold[Task[Either[AuthServiceError, AuthToken]]](
          UserNotFound.asLeft[AuthToken].pure[Task]
        ) { user =>
          verifier.verify(credentials.password, user.passwordHash).flatMap {
            isVerified =>
              if (isVerified) {
                val token = tokenGenerator.generate
                sessionCache
                  .put(token.value)(user.mapTo[AuthorizedUser])
                  .map(_ => token.asRight)
              } else {
                UserNotFound.asLeft[AuthToken].pure[Task]
              }
          }
        }
      }

  override def getAuthorizedUser(
    token: AuthToken
  ): Task[Option[AuthorizedUser]] =
    sessionCache.get(token.value)
}
