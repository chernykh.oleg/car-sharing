package logic.AuthService.AuthTokenGenerator

import api.UsersApi.AuthEndpoint.AuthToken

import java.util.UUID

class AuthTokenGeneratorImpl extends AuthTokenGenerator {
  override def generate: AuthToken =
    AuthToken(value = UUID.randomUUID().toString)
}
