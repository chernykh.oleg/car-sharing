package logic.AuthService.AuthTokenGenerator

import api.UsersApi.AuthEndpoint.AuthToken

trait AuthTokenGenerator {
  def generate: AuthToken
}
