package logic.AuthService

import api.UsersApi.AuthEndpoint.{AuthToken, HasAuthToken}
import api.UsersApi.LogInEndpoint.Credentials
import cats.implicits.{catsSyntaxApplicativeId, catsSyntaxEitherId}
import io.circe.generic.JsonCodec
import monix.eval.Task
import sttp.model.StatusCode
import sttp.model.StatusCode._

object AuthService {
  @JsonCodec
  case class AuthorizedUser(id: Long)
}

trait AuthService {
  import AuthService._

  def authorize(
    credentials: Credentials
  ): Task[Either[AuthServiceError, AuthToken]]

  def getAuthorizedUser(token: AuthToken): Task[Option[AuthorizedUser]]

  final def extract[R <: HasAuthToken, T](incomeRequestData: R)(
    andThen: AuthorizedUser => Task[Either[StatusCode, T]]
  ): Task[Either[StatusCode, T]] = {
    getAuthorizedUser(incomeRequestData.authToken).flatMap { userOpt =>
      userOpt
        .fold(Unauthorized.asLeft[T].pure[Task])(andThen)
    }
  }

}
