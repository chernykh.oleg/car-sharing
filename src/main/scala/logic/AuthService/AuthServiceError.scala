package logic.AuthService

sealed trait AuthServiceError extends Throwable

object AuthServiceError {
  object UserNotFound extends AuthServiceError
}
