package logic.CarStatesService

import api.UsersApi.GetCarsToBookEndpoint.GeoPoint
import monix.eval.Task

import java.util.Date

object CarStatesService {
  case class CarState(id: Long = 0L,
                      carId: Long,
                      fuel: Double,
                      isLock: Boolean,
                      speed: Int,
                      geoPoint: GeoPoint,
                      generatedAt: Date)
}

trait CarStatesService {
  import CarStatesService._

  def insertCarStates(batchOfState: Seq[CarState]): Task[Unit]
}
