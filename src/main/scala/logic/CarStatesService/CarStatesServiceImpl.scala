package logic.CarStatesService

import com.typesafe.scalalogging.LazyLogging
import db.CarStates.{CarStateModel, CarStatesRepository}
import db.DBComponent
import io.scalaland.chimney.dsl.TransformerOps
import logic.CarStatesService.CarStatesService.CarState
import monix.eval.Task
import utils.DateUtils.dateOps
import utils.TypeMapper
import utils.TypeMapper.TypeMapperOps

object CarStatesServiceImpl {
  implicit val mapperTo: TypeMapper[CarState, CarStateModel] =
    TypeMapper.instance[CarState, CarStateModel](
      _.into[CarStateModel]
        .withFieldComputed(_.latitude, _.geoPoint.latitude)
        .withFieldComputed(_.longitude, _.geoPoint.longitude)
        .withFieldComputed(_.generatedAt, _.generatedAt.toDBTimeRepr)
        .transform
    )
}

class CarStatesServiceImpl(carStatesRepository: CarStatesRepository)
    extends CarStatesService
    with LazyLogging {
  this: DBComponent =>

  import CarStatesServiceImpl._

  override def insertCarStates(batchOfStates: Seq[CarState]): Task[Unit] =
    Task
      .pure(batchOfStates.map(_.mapTo[CarStateModel]))
      .flatMap { batch =>
        Task
          .deferFuture {
            db.run {
              carStatesRepository.insertStates(batch)
            }
          }
      }
      .void
}
