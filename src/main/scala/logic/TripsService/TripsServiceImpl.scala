package logic.TripsService
import cats.data.{NonEmptyList, OptionT}
import cats.implicits.{
  catsStdInstancesForFuture,
  catsSyntaxApplicativeId,
  catsSyntaxEitherId,
  catsSyntaxOptionId
}
import db.Books.BooksRepository
import db.CarStates.{CarStateModel, CarStatesRepository}
import db.Cars.CarsRepository
import db.DBComponent
import db.Trips.{TripModel, TripsRepository}
import io.scalaland.chimney.dsl.TransformerOps
import logic.TripsService.TripServiceError.{
  BookNotFound,
  FindActiveTripError,
  FinishTripError,
  StartTripError,
  TripNotFound
}
import logic.TripsService.TripsService.{Trip, TripResult}
import monix.eval.Task
import slick.dbio.DBIOAction
import utils.CommonUtils.eitherOps
import utils.DateUtils.{dateOps, longOps}
import utils.TypeMapper
import utils.TypeMapper.TypeMapperOps

import java.util.Date
import java.util.concurrent.TimeUnit
import scala.concurrent.duration.{Duration, FiniteDuration}
import scala.util.control.NonFatal
import scala.util.{Failure, Success}

object TripsServiceImpl {
  implicit val mapper: TypeMapper[TripModel, Trip] =
    TypeMapper.instance[TripModel, Trip](
      _.into[Trip]
        .withFieldComputed(_.startAt, _.startAt.toDate)
        .withFieldComputed(_.finishAt, _.finishAt.map(_.toDate))
        .transform
    )

  implicit class carStatesOpt(private val states: Seq[CarStateModel])
      extends AnyVal {
    private def getDistanceBetween(state1: CarStateModel,
                                   state2: CarStateModel): Double =
      Math.sqrt(
        Math.pow(state1.latitude - state2.latitude, 2) + Math
          .pow(state1.longitude - state2.longitude, 2)
      )
    def coveredDistance: Double =
      states
        .sliding(2)
        .foldLeft(0D)((acc, pair) => {
          acc + getDistanceBetween(pair.head, pair.tail.head)
        })
    def duration: FiniteDuration = {
      if (states.length <= 1) Duration.Zero
      else
        states.last.generatedAt.toFiniteDuration - states.head.generatedAt.toFiniteDuration
    }
  }
}
class TripsServiceImpl(tripsRepository: TripsRepository,
                       booksRepository: BooksRepository,
                       carsRepository: CarsRepository,
                       carStatesRepository: CarStatesRepository)
    extends TripsService { this: DBComponent =>

  import TripsServiceImpl._
  import driver.api._

  override def findActiveTripByUserId(userId: Long): Task[Option[TripResult]] =
    Task
      .deferFutureAction { implicit scheduler =>
        db.run {
          tripsRepository
            .findActiveTripByUserId(userId)
            .flatMap[Option[TripResult], NoStream, Nothing] {
              case Some(trip) if trip.finishAt.isEmpty => {
                carStatesRepository
                  .findStatesInInterval(
                    trip.carId,
                    trip.startAt,
                    new Date().toDBTimeRepr
                  )
                  .map(makeTripResult(trip, _).some)
              }
              case _ => DBIOAction.successful(None)
            }
        }
      }

  override def startTrip(userId: Long): Task[Either[TripServiceError, Long]] =
    Task
      .deferFutureAction { implicit scheduler =>
        db.run {
          booksRepository
            .findBookByUserId(userId)
            .flatMap[Long, NoStream, Effect.Write with Effect.Read] {
              case Some(book) => {
                booksRepository
                  .deleteBookById(book.id)
                  .flatMap[Long, NoStream, Effect.Write] {
                    case 1 => {
                      val tripModel =
                        makeTripModel(book.userId, book.carId)
                      tripsRepository.insertTrip(tripModel)
                    }
                    case _ =>
                      DBIOAction.failed(StartTripError("Ошибка удаления брони"))
                  }
              }
              case None => DBIOAction.failed(BookNotFound)
            }
            .transactionally
            .asTry
        }
      }
      .flatMap {
        case Success(tripId)              => tripId.asRight.pure[Task]
        case Failure(e: TripServiceError) => e.asLeft.pure[Task]
        case Failure(NonFatal(e)) =>
          StartTripError(e.getMessage).asLeft.pure[Task]
        case Failure(e) => Task.raiseError(e)
      }

  override def finishTrip(
    userId: Long
  ): Task[Either[TripServiceError, TripsService.TripResult]] =
    Task
      .deferFutureAction { implicit scheduler =>
        db.run {
          tripsRepository
            .findActiveTripByUserId(userId)
            .flatMap {
              case Some(trip) if trip.finishAt.isEmpty => {
                val finishAt = new Date().toDBTimeRepr
                tripsRepository
                  .insertOrUpdateTrip(trip.copy(finishAt = finishAt.some))
                  .flatMap {
                    case 0 =>
                      DBIOAction.failed(
                        FinishTripError("Не удалось обновить поездку")
                      )
                    case 1 => {
                      carsRepository
                        .findCarById(trip.carId, blockReturned = true)
                        .flatMap {
                          case Some(car) => {
                            carsRepository.updateCar(
                              car.copy(availableToUse = true)
                            ) >>
                              carStatesRepository
                                .findStatesInInterval(
                                  trip.carId,
                                  trip.startAt,
                                  finishAt
                                )
                                .map(makeTripResult(trip, _))
                          }
                          case None => DBIOAction.failed(FinishTripError())
                        }
                    }
                    case _ =>
                      DBIOAction.failed(
                        FinishTripError("Ошибка при обновлении поездки")
                      )
                  }
              }
              case _ => DBIOAction.failed(TripNotFound)
            }
            .transactionally
            .asTry
        }
      }
      .flatMap {
        case Success(v)                   => v.asRight.pure[Task]
        case Failure(e: TripServiceError) => e.asLeft.pure[Task]
        case Failure(NonFatal(e)) =>
          FinishTripError(e.getMessage).asLeft.pure[Task]
        case Failure(e) => Task.raiseError(e)
      }

  private def makeTripModel(userId: Long, carId: Long): TripModel =
    TripModel(
      userId = userId,
      carId = carId,
      startAt = new Date().toDBTimeRepr,
      finishAt = None
    )

  private def makeTripResult(trip: TripModel,
                             carStates: Seq[CarStateModel]): TripResult = {
    val sortedCarStates = carStates.sortBy(_.generatedAt)
    val tripDurationInMinutes =
      sortedCarStates.duration.toMinutes.toInt
    TripResult(
      tripId = trip.id,
      startedAt = trip.startAt.toDate,
      totalDistance = sortedCarStates.coveredDistance,
      tripDurationInMinutes = tripDurationInMinutes,
      costPerMinute = trip.costPerMinute,
      totalCost = tripDurationInMinutes * trip.costPerMinute
    )
  }
}
