package logic.TripsService

import logic.TripsService.TripsService.{Trip, TripResult}
import monix.eval.Task

import java.util.Date

object TripsService {
  case class Trip(id: Long = 0L,
                  userId: Long,
                  carId: Long,
                  costPerMinute: Double,
                  startAt: Date,
                  finishAt: Option[Date])

  case class TripResult(tripId: Long,
                        startedAt: Date,
                        totalDistance: Double,
                        tripDurationInMinutes: Int,
                        costPerMinute: Double,
                        totalCost: Double)
}

trait TripsService {
  def findActiveTripByUserId(userId: Long): Task[Option[TripResult]]
  def startTrip(userId: Long): Task[Either[TripServiceError, Long]]
  def finishTrip(userId: Long): Task[Either[TripServiceError, TripResult]]
}
