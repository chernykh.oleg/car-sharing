package logic.TripsService

sealed trait TripServiceError extends Throwable

object TripServiceError {
  object TripNotFound extends TripServiceError
  object BookNotFound extends TripServiceError
  case class FindActiveTripError(info: String = "") extends TripServiceError
  case class StartTripError(info: String = "") extends TripServiceError
  case class FinishTripError(info: String = "") extends TripServiceError
}
