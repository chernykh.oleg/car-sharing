package logic.CarsService

sealed trait CarsServiceError extends Throwable

object CarsServiceError {
  object CarNotFound extends CarsServiceError
  case class UpdatingCarError(what: String = "") extends CarsServiceError
}
