package logic.CarsService

import api.UsersApi.GetCarsToBookEndpoint.{GeoPoint, SearchArea}
import monix.eval.Task

object CarsService {
  case class CarWithState(id: Long,
                          vin: String,
                          brand: String,
                          model: String,
                          number: String,
                          fuel: Double,
                          geoPoint: GeoPoint)

  case class Car(id: Long,
                 vin: String,
                 brand: String,
                 model: String,
                 number: String)
}

trait CarsService {
  import CarsService._

  def makeCarAvailableToUse(carId: Long): Task[Either[CarsServiceError, Unit]]
  def removeCarFromUse(carId: Long): Task[Either[CarsServiceError, Unit]]
  def findCarsToBook(searchArea: SearchArea): Task[List[CarWithState]]
  def findCarByVIN(vin: String): Task[Option[Car]]
}
