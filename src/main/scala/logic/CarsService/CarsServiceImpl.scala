package logic.CarsService

import api.UsersApi.GetCarsToBookEndpoint
import api.UsersApi.GetCarsToBookEndpoint.GeoPoint
import cats.data.OptionT
import cats.implicits.{catsSyntaxApplicativeId, catsSyntaxEitherId}
import db.CarStates.CarStatesRepository
import db.Cars.{CarModel, CarsRepository}
import db.DBComponent
import logic.CarsService.CarsService.{Car, CarWithState}
import monix.eval.Task
import utils.TypeMapper
import utils.TypeMapper.TypeMapperOps
import io.scalaland.chimney.dsl.TransformerOps
import logic.CarsService.CarsServiceError._
import slick.dbio.DBIOAction

import scala.util.control.NonFatal
import scala.util.{Failure, Success}

object CarsServiceImpl {
  implicit val mapperFrom: TypeMapper[CarModel, Car] =
    TypeMapper.instance[CarModel, Car](_.into[Car].transform)
}

class CarsServiceImpl(carsRepository: CarsRepository,
                      carStatesRepository: CarStatesRepository)
    extends CarsService {
  this: DBComponent =>

  import CarsServiceImpl._

  override def findCarsToBook(
    searchArea: GetCarsToBookEndpoint.SearchArea
  ): Task[List[CarsService.CarWithState]] = {
    Task
      .deferFuture(
        db.run(carStatesRepository.findStatesOfCarsInArea(searchArea))
      )
      .flatMap { carStates =>
        Task
          .deferFuture(
            db.run(
              carsRepository
                .findAvailableCarsToBookByIds(carStates.map(_.carId).toList)
            )
          )
          .map { availableCarsToBook =>
            val carIdToState = carStates.map(c => c.carId -> c).toMap
            availableCarsToBook
              .foldLeft(List.empty[CarWithState])((acc, car) => {
                carIdToState.get(car.id).fold(acc) { state =>
                  CarWithState(
                    id = car.id,
                    vin = car.vin,
                    brand = car.brand,
                    model = car.model,
                    number = car.number,
                    fuel = state.fuel,
                    geoPoint = GeoPoint(state.latitude, state.longitude)
                  ) +: acc
                }
              })
          }
      }
  }

  override def findCarByVIN(vin: String): Task[Option[Car]] =
    OptionT {
      Task
        .deferFuture {
          db.run {
            carsRepository.findCarByVIN(vin)
          }
        }
    }.map(_.mapTo[Car]).value

  override def makeCarAvailableToUse(
    carId: Long
  ): Task[Either[CarsServiceError, Unit]] =
    updateCarAvailability(carId, availability = true)

  override def removeCarFromUse(
    carId: Long
  ): Task[Either[CarsServiceError, Unit]] =
    updateCarAvailability(carId, availability = false)

  private def updateCarAvailability(
    carId: Long,
    availability: Boolean
  ): Task[Either[CarsServiceError, Unit]] = {
    Task
      .deferFutureAction { implicit scheduler =>
        db.run {
          carsRepository
            .findCarById(carId, blockReturned = true)
            .flatMap {
              case Some(car) =>
                carsRepository.updateCar(
                  car.copy(availableToUse = availability)
                )
              case None => DBIOAction.failed(CarNotFound)
            }
            .asTry
        }
      }
      .flatMap {
        case Success(result) if result == 1 => ().asRight.pure[Task]
        case Success(_)                     => UpdatingCarError().asLeft.pure[Task]
        case Failure(e: CarsServiceError)   => e.asLeft.pure[Task]
        case Failure(NonFatal(e)) =>
          UpdatingCarError(e.getMessage).asLeft.pure[Task]
        case Failure(e) => Task.raiseError(e)
      }
  }
}
