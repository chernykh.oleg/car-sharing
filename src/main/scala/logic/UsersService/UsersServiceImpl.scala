package logic.UsersService

import api.UsersApi.SignInEndpoint
import cats.data.OptionT
import cats.implicits.{catsSyntaxApplicativeId, catsSyntaxEitherId}
import db.DBComponent
import db.Users.{UserModel, UsersRepository}
import io.scalaland.chimney.dsl.TransformerOps
import logic.UsersService.UsersService.User
import logic.UsersService.UsersServiceError.{
  RegistrationError,
  UserAlreadyExist,
  Validation
}
import logic.UsersService.Validator.Validator
import monix.eval.Task
import utils.PasswordHashing.PasswordHasher
import utils.TypeMapper
import utils.TypeMapper.TypeMapperOps

import scala.util.control.NonFatal
import scala.util.{Failure, Success}

object UsersServiceImpl {
  implicit val mapper1: TypeMapper[UserModel, User] =
    TypeMapper.instance[UserModel, User](_.into[User].transform)
}

class UsersServiceImpl(hasher: PasswordHasher,
                       usersRepository: UsersRepository,
                       validator: Validator)
    extends UsersService { this: DBComponent =>
  import UsersServiceImpl._

  override def findUserByLogin(login: String): Task[Option[UsersService.User]] =
    OptionT {
      Task.deferFuture {
        db.run(usersRepository.findUserByLogin(login))
      }
    }.map(_.mapTo[User]).value

  override def registerUser(
    registration: SignInEndpoint.UserRegistration
  ): Task[Either[UsersServiceError, Long]] = {
    Task
      .pure(validator(registration))
      .flatMap { validation =>
        if (validation.isAnyInvalid) Task.pure(Failure(validation))
        else
          hasher
            .hash(registration.password)
            .flatMap { passwordHash =>
              Task.deferFuture {
                db.run {
                  usersRepository
                    .insertUser(
                      registration
                        .into[UserModel]
                        .withFieldConst(_.passwordHash, passwordHash)
                        .transform
                    )
                    .asTry
                }
              }
            }
      }
      .flatMap {
        case Success(v)             => v.asRight.pure[Task]
        case Failure(e: Validation) => e.asLeft.pure[Task]
        case Failure(NonFatal(e)) if e.getMessage.contains("duplicate") =>
          UserAlreadyExist.asLeft.pure[Task]
        case Failure(NonFatal(e)) =>
          RegistrationError(e.getMessage).asLeft.pure[Task]
        case Failure(e) => Task.raiseError(e)
      }
  }
}
