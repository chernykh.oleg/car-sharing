package logic.UsersService

import api.UsersApi.SignInEndpoint.UserRegistration
import monix.eval.Task

object UsersService {
  case class User(id: Long,
                  login: String,
                  passwordHash: String,
                  email: String,
                  passportNumber: String)
}

trait UsersService {
  import UsersService._

  def findUserByLogin(login: String): Task[Option[User]]

  def registerUser(
    registration: UserRegistration
  ): Task[Either[UsersServiceError, Long]]
}
