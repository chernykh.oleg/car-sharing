package logic.UsersService

import api.UsersApi.SignInEndpoint.ValidationError

sealed trait UsersServiceError extends Throwable

object UsersServiceError {
  case class Validation(err: ValidationError) extends UsersServiceError {
    lazy val isAnyInvalid
      : Boolean = err.email.isDefined || err.login.isDefined || err.password.isDefined || err.passportNumber.isDefined
  }
  object UserAlreadyExist extends UsersServiceError
  case class RegistrationError(reason: String) extends UsersServiceError
}
