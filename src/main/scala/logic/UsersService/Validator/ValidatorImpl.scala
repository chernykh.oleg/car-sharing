package logic.UsersService.Validator

import api.UsersApi.SignInEndpoint
import api.UsersApi.SignInEndpoint.ValidationError
import cats.implicits.catsSyntaxOptionId
import logic.UsersService.UsersServiceError._
import org.apache.commons.lang3.StringUtils
import SignInEndpoint._

object ValidatorImpl extends Validator {
  override def apply(registration: UserRegistration): Validation = Validation(
    err = ValidationError(
      login = validateLogin(registration.login),
      password = validatePassword(registration.password),
      email = validateEmail(registration.email),
      passportNumber = validatePassportNumber(registration.passportNumber)
    )
  )

  private def validatePassword(password: String): Option[String] = {
    if (!StringUtils.isNotBlank(password)) "пароль не должен быть пустым".some
    else if (password.contains(' ')) "пароль не должен содержать пробелы".some
    else if (password.length < 5) "пароль не должен быть короче 5 символов".some
    else None
  }

  private def validateLogin(login: String): Option[String] = {
    if (!StringUtils.isNotBlank(login)) "логин не должен быть пустым".some
    else if (login.contains(' ')) "логин не должен содержать пробелы".some
    else if (login.length < 5) "логин не должен быть короче 5 символов".some
    else None
  }

  private def validateEmail(email: String): Option[String] = {
    if (!StringUtils.isNotBlank(email)) "email не должен быть пустым".some
    // custom pattern, as an example
    else if (!email.matches("^\\w+@test.test$")) "несуществующий email".some
    else None
  }

  private def validatePassportNumber(number: String): Option[String] = {
    if (!number.matches("^\\d{10}$"))
      "некорректный номер паспорта".some
    else None
  }
}
