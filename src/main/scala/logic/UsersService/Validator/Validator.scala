package logic.UsersService.Validator

import api.UsersApi.SignInEndpoint.UserRegistration
import logic.UsersService.UsersServiceError.Validation

trait Validator {
  def apply(registration: UserRegistration): Validation
}
