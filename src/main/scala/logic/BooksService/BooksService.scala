package logic.BooksService

import monix.eval.Task

import java.time.Instant
import java.util.{Calendar, Date}
import java.util.concurrent.TimeUnit
import scala.concurrent.duration.FiniteDuration

object BooksService {
  case class Book(id: Long = 0L,
                  userId: Long,
                  carId: Long,
                  openedAt: Date,
                  duration: FiniteDuration) {
    def timeToClose(): FiniteDuration = {
      val currentTime = new Date().getTime
      val millisLeft = openedAt.getTime + duration.toMillis - currentTime
      FiniteDuration(millisLeft, TimeUnit.MILLISECONDS)
    }
  }

}

trait BooksService {
  import BooksService._

  def findBookById(bookId: Long): Task[Option[Book]]

  def findBookByUserId(userId: Long): Task[Option[Book]]

  def findBooksExpiredIn(duration: FiniteDuration): Task[List[Book]]

  def closeBookByUserId(userId: Long): Task[Either[BookServiceError, Unit]]

  def bookCar(userId: Long, carId: Long): Task[Either[BookServiceError, Book]]
}
