package logic.BooksService
import cats.data.{EitherT, OptionT}
import cats.implicits.{catsSyntaxApplicativeId, catsSyntaxEitherId}
import com.typesafe.scalalogging.LazyLogging
import db.Books.{BookModel, BooksRepository}
import db.Cars.CarsRepository
import db.DBComponent
import io.scalaland.chimney.dsl.TransformerOps
import logic.BooksService.BookServiceError.{
  BookCarError,
  BookNotFound,
  CarIsNotAvailableToUse,
  CloseBookError
}
import logic.BooksService.BooksService.Book
import monix.eval.Task
import server.AppConfig.BookConfig
import slick.dbio.DBIOAction
import utils.DateUtils.{dateOps, durationOps, longOps}
import utils.TypeMapper
import utils.TypeMapper.TypeMapperOps

import java.util.Date
import scala.concurrent.duration.FiniteDuration
import scala.util.control.NonFatal
import scala.util.{Failure, Success}

object BooksServiceImpl {
  implicit val mapper: TypeMapper[BookModel, Book] =
    TypeMapper.instance[BookModel, Book](
      _.into[Book]
        .withFieldComputed(_.openedAt, b => new Date(b.openedAt))
        .withFieldComputed(_.duration, _.duration.toFiniteDuration)
        .transform
    )
}

class BooksServiceImpl(config: BookConfig,
                       booksRepository: BooksRepository,
                       carsRepository: CarsRepository)
    extends BooksService
    with LazyLogging { this: DBComponent =>

  import driver.api._

  import BooksServiceImpl._

  override def findBookByUserId(userId: Long): Task[Option[Book]] =
    OptionT {
      Task.deferFuture {
        db.run(booksRepository.findBookByUserId(userId))
      }
    }.map(_.mapTo[Book]).value

  override def findBookById(bookId: Long): Task[Option[BooksService.Book]] =
    OptionT {
      Task.deferFuture {
        db.run(booksRepository.findBookById(bookId))
      }
    }.map(_.mapTo[Book]).value

  override def findBooksExpiredIn(
    duration: FiniteDuration
  ): Task[List[BooksService.Book]] =
    Task
      .deferFuture {
        db.run(booksRepository.findBooksExpiredIn(duration))
      }
      .map { books =>
        books.map(_.mapTo[Book])
      }
      .map(_.toList)

  override def closeBookByUserId(
    userId: Long
  ): Task[Either[BookServiceError, Unit]] =
    Task
      .deferFutureAction { implicit scheduler =>
        db.run {
          booksRepository
            .findBookByUserId(userId)
            .flatMap {
              case Some(book) => {
                carsRepository
                  .findCarById(book.carId, blockReturned = true)
                  .flatMap {
                    case Some(car) =>
                      carsRepository
                        .updateCar(car = car.copy(availableToUse = true)) >> booksRepository
                        .deleteBookById(book.id)
                    case None => DBIOAction.failed(CloseBookError())
                  }
              }
              case None => DBIOAction.failed(BookNotFound)
            }
            .transactionally
            .asTry
        }
      }
      .flatMap {
        case Success(1)                   => ().asRight.pure[Task]
        case Success(0)                   => BookNotFound.asLeft.pure[Task]
        case Success(_)                   => CloseBookError().asLeft.pure[Task]
        case Failure(e: BookServiceError) => e.asLeft.pure[Task]
        case Failure(NonFatal(_))         => CloseBookError().asLeft.pure[Task]
        case Failure(e)                   => Task.raiseError(e)
      }

  override def bookCar(
    userId: Long,
    carId: Long
  ): Task[Either[BookServiceError, BooksService.Book]] = {
    Task
      .deferFutureAction { implicit scheduler =>
        db.run {
          carsRepository
            .findCarById(carId)
            .flatMap[Book, NoStream, Nothing] {
              case Some(car) if car.availableToUse => {
                val bookModel = makeBookModel(userId, carId)
                carsRepository
                  .updateCar(car.copy(availableToUse = false)) >>
                  booksRepository
                    .insertBook(bookModel)
                    .map(makeBook(_, bookModel))
              }
              case _ => DBIOAction.failed(CarIsNotAvailableToUse)
            }
            .transactionally
            .asTry
        }
      }
      .flatMap {
        case Success(book)                => book.asRight.pure[Task]
        case Failure(e: BookServiceError) => e.asLeft.pure[Task]
        case Failure(NonFatal(e)) =>
          BookCarError(e.getMessage).asLeft.pure[Task]
        case Failure(e) => Task.raiseError(e)
      }

  }

  private def makeBookModel(userId: Long, carId: Long) = BookModel(
    userId = userId,
    carId = carId,
    openedAt = new Date().toDBTimeRepr,
    duration = config.duration.toDBTimeRepr,
    costPerMinute = config.costPerMinute
  )

  private def makeBook(bookId: Long, insertedBookDB: BookModel): Book =
    insertedBookDB.mapTo[Book].copy(id = bookId)

}
