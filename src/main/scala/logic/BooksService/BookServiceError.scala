package logic.BooksService

sealed trait BookServiceError extends Throwable

object BookServiceError {
  object BookNotFound extends BookServiceError
  object CarIsNotAvailableToUse extends BookServiceError
  case class CloseBookError(info: String = "") extends BookServiceError
  case class BookCarError(info: String = "") extends BookServiceError
}
