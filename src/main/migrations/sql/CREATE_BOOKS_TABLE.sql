CREATE TABLE books (
	id SERIAL PRIMARY KEY,
	user_id INTEGER UNIQUE NOT NULL,
	car_id INTEGER UNIQUE NOT NULL,
	opened_at BIGINT NOT NULL,
	book_duration BIGINT NOT NULL,
	cost_per_minute INTEGER NOT NULL,
	CONSTRAINT fk_user_id
        FOREIGN KEY(user_id)
            REFERENCES users(id),
    CONSTRAINT fk_car_id
        FOREIGN KEY(car_id)
            REFERENCES cars(id)
);