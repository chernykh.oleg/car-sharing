CREATE TABLE car_states (
	id SERIAL PRIMARY KEY,
	car_id INTEGER,
	fuel REAL,
	is_lock BOOLEAN,
	speed INTEGER,
	latitude REAL,
	longitude REAL,
	generated_at BIGINT,
	CONSTRAINT fk_car_id
        FOREIGN KEY(car_id)
    	    REFERENCES cars(id)
);