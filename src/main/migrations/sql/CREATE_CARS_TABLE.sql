CREATE TABLE cars (
	id SERIAL PRIMARY KEY,
	vin VARCHAR ( 10 ) UNIQUE NOT NULL,
	number VARCHAR ( 10 ) UNIQUE NOT NULL,
	brand VARCHAR ( 32 ) NOT NULL,
	model VARCHAR ( 32 ) NOT NULL,
	available_to_use BOOLEAN NOT NULL
);