CREATE TABLE users (
	id serial PRIMARY KEY,
	login VARCHAR ( 50 ) UNIQUE NOT NULL,
	password_hash VARCHAR ( 255  ) NOT NULL,
	email VARCHAR ( 255 ) UNIQUE NOT NULL,
	passport_number VARCHAR ( 10 ) UNIQUE NOT NULL
);

CREATE TABLE cars (
	id SERIAL PRIMARY KEY,
	vin VARCHAR ( 10 ) UNIQUE NOT NULL,
	number VARCHAR ( 10 ) UNIQUE NOT NULL,
	brand VARCHAR ( 32 ) NOT NULL,
	model VARCHAR ( 32 ) NOT NULL,
	available_to_use BOOLEAN NOT NULL
);

CREATE TABLE books (
	id SERIAL PRIMARY KEY,
	user_id INTEGER UNIQUE NOT NULL,
	car_id INTEGER UNIQUE NOT NULL,
	opened_at BIGINT NOT NULL,
	book_duration BIGINT NOT NULL,
	cost_per_minute INTEGER NOT NULL,
	CONSTRAINT fk_user_id
        FOREIGN KEY(user_id)
            REFERENCES users(id),
    CONSTRAINT fk_car_id
        FOREIGN KEY(car_id)
            REFERENCES cars(id)
);

CREATE TABLE trips (
	id SERIAL PRIMARY KEY,
	user_id INTEGER NOT NULL,
	car_id INTEGER NOT NULL,
	cost_per_minute INTEGER NOT NULL,
	start_at BIGINT NOT NULL,
	finish_at BIGINT,
	CONSTRAINT fk_user_id
        FOREIGN KEY(user_id)
            REFERENCES users(id),
    CONSTRAINT fk_car_id
        FOREIGN KEY(car_id)
            REFERENCES cars(id)
);

CREATE TABLE car_states (
	id SERIAL PRIMARY KEY,
	car_id INTEGER,
	fuel REAL,
	is_lock BOOLEAN,
	speed INTEGER,
	latitude REAL,
	longitude REAL,
	generated_at BIGINT,
	CONSTRAINT fk_car_id
        FOREIGN KEY(car_id)
    	    REFERENCES cars(id)
);