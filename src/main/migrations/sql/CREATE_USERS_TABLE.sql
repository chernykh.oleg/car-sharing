CREATE TABLE users (
	id serial PRIMARY KEY,
	login VARCHAR ( 50 ) UNIQUE NOT NULL,
	password_hash VARCHAR ( 255  ) NOT NULL,
	email VARCHAR ( 255 ) UNIQUE NOT NULL,
	passport_number VARCHAR ( 10 ) UNIQUE NOT NULL
);