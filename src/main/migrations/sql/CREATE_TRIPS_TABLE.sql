CREATE TABLE trips (
	id SERIAL PRIMARY KEY,
	user_id INTEGER NOT NULL,
	car_id INTEGER NOT NULL,
	cost_per_minute INTEGER NOT NULL,
	start_at BIGINT NOT NULL,
	finish_at BIGINT,
	CONSTRAINT fk_user_id
        FOREIGN KEY(user_id)
            REFERENCES users(id),
    CONSTRAINT fk_car_id
        FOREIGN KEY(car_id)
            REFERENCES cars(id)
);